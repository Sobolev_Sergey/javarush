/*Максимум четырех чисел
Ввести с клавиатуры четыре числа, и вывести максимальное из них.
Если числа равны между собой, необходимо вывести любое.
*/
import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int a = Integer.parseInt(reader.readLine());
        int b = Integer.parseInt(reader.readLine());
        int c = Integer.parseInt(reader.readLine());
        int d = Integer.parseInt(reader.readLine());
        compare(a,b,c,d);
    }
    
    public static void compare(int a,int b,int c, int d) {
        if(a>b && a>c && a>d)
            System.out.println(a);
        else if(b>a && b>c && b>d)
            System.out.println(b);
        else if(c>a && c>b && c>d)
            System.out.println(c);
        else if(d>a && d>b && d>c)
            System.out.println(d);
        else
            System.out.println(a);
    }
}

/*
Сортировка трех чисел
Ввести с клавиатуры три числа, и вывести их в порядке убывания.
Выведенные числа должны быть разделены пробелом.
*/
package com.javarush.task.task04.task0420;

/* 
Сортировка трех чисел
*/

import java.io.*;
import java.util.Arrays;


public class Solution {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Integer a = Integer.parseInt(reader.readLine());
        Integer b = Integer.parseInt(reader.readLine());
        Integer c = Integer.parseInt(reader.readLine());
        int[] mas = {a, b, c};
        Arrays.sort(mas);
        System.out.println(mas[2] + " " + mas[1] + " " + mas[0]);
    }
}


/*
import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //Scanner sc = new Scanner(System.in);
        //int a = sc.nextInt();
        //int b = sc.nextInt();
        //int c = sc.nextInt();
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int a = Integer.parseInt(reader.readLine());
        int b = Integer.parseInt(reader.readLine());
        int c = Integer.parseInt(reader.readLine());
        
        int[] arr = new int [3];
        arr[0] = a;
        arr[1] = b;
        arr[2] = c;
        arr = bubbleSort(arr);
        System.out.println(arr[0] + " " + arr[1] + " " + arr[2]);
    }

    public static int[] bubbleSort(int[] arr){
        for(int i = 0; i < arr.length-1; i++ ){
            if(arr[i+1] > arr[i] ){
                int tmp = arr[i];
                arr[i] = arr[i+1];
                arr[i+1] = tmp;
            }
        }
        return arr;
    }
}

*/
/*
public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
         BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int a = Integer.parseInt(reader.readLine());
        int b = Integer.parseInt(reader.readLine());
        int c = Integer.parseInt(reader.readLine());
        compare(a,b,c);
    }
    
        public static void compare(int a,int b,int c){
            String result = "";
            if ((a>b) && (a>c)) {
                result += a;
                if (b > c)
                    result += " " + b + " " + c;
                else if (c > b)
                    result += " " + c + " " + b;
                //else
                //    result += " " + c + " " + b;
            } else if ((b>a) && (b>c)) {
                result += b;
                if (a > c)
                    result += " " + a + " " + c;
                else if (c > a)
                    result += " " + c + " " + a;
            }else if ((c>a) && (c>b)) {
                result += c;
                if (a > b)
                    result += " " + a + " " + b;
                else if (b > a)
                    result += " " + b + " " + a;
            }
            System.out.println(result);
    }
}
*/

package com.javarush.task.task04.task0421;

/* 
Настя или Настя?
Ввести с клавиатуры два имени, и если имена одинаковые вывести сообщение «Имена идентичны».
Если имена разные, но их длины равны – вывести сообщение – «Длины имен равны».
Если имена и длины имен разные — ничего не выводить.

*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        BufferedReader s = new BufferedReader(new InputStreamReader(System.in));

        String name1 = s.readLine();
        String name2 = s.readLine();

        if (name1.equals(name2)) {
            System.out.println("Имена идентичны");
        } else if (name1.length() == name2.length()) {
            System.out.println("Длины имен равны");
        }
        
        /*
        String s1= reader.readLine();
        String s2= reader.readLine();
        int length1 = s1.length();
        int length2 = s2.length();

        if (s1.equals(s2)) {
            System.out.println("Имена идентичны");
        }else {
            if (length1==length2) {
                System.out.println("Длины имен равны");
            }

        }*/
    }
}

package com.javarush.task.task04.task0422;

/* 
18+
Ввести с клавиатуры имя и возраст. Если возраст меньше 18 вывести надпись «Подрасти еще».
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        BufferedReader s = new BufferedReader(new InputStreamReader(System.in));
        String name = s.readLine();
        
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Integer age = Integer.parseInt(reader.readLine());


        if (age<18) {
            System.out.println("Подрасти еще");
        }
    }
}


package com.javarush.task.task04.task0423;

/* 
Фейс-контроль
Ввести с клавиатуры имя и возраст. Если возраст больше 20 вывести надпись «И 18-ти достаточно».
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        BufferedReader s = new BufferedReader(new InputStreamReader(System.in));
        String name = s.readLine();
        
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Integer age = Integer.parseInt(reader.readLine());


        if (age>20) {
            System.out.println("И 18-ти достаточно");
        }
    }
}


package com.javarush.task.task04.task0424;

/* 
Три числа
Три числа
Ввод с клавиатуры, сравнение чисел и вывод на экран — у студентов 4 уровня секретного 
центра JavaRush эти навыки оттачиваются до автоматизма.
Давайте напишем программу, в которой пользователь вводит три числа с клавиатуры. 
Затем происходит сравнение, и если мы находим число, которое отличается от двух 
других, выводим на экран его порядковый номер.
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
//напишите тут ваш код
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Integer a = Integer.parseInt(reader.readLine());
        Integer b = Integer.parseInt(reader.readLine());
        Integer c = Integer.parseInt(reader.readLine());

        if(a!=b && b==c)
            System.out.println(1);
        else if(b!=a && a==c)
            System.out.println(2);
        else if(c!=a && a==b)
            System.out.println(3);
    }
}


package com.javarush.task.task04.task0425;

/* 
Цель установлена!
Ввести с клавиатуры два целых числа, которые будут координатами точки, не лежащей на координатных осях OX и OY.
Вывести на экран номер координатной четверти, в которой находится данная точка.
Подсказка:
Принадлежность точки с координатами (a,b) к одной из четвертей определяется следующим образом:
для первой четверти a>0 и b>0;
для второй четверти a<0 и b>0;
для третьей четверти a<0 и b<0;
для четвертой четверти a>0 и b<0.

*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Integer a = Integer.parseInt(reader.readLine());
        Integer b = Integer.parseInt(reader.readLine());
        
        if(a>0 && b>0){
            System.out.println(1);
        }
        if(a<0 && b>0){
            System.out.println(2);
        }
        if(a<0 && b<0){
            System.out.println(3);
        }
        if(a>0 && b<0){
            System.out.println(4);
        }
    }
}

Тип boolean ---------------------------------------------------------------------------------------------------------------------------------------------------------------
boolean m;
Два данных выражения эквивалентны. Значение переменой типа boolean по умолчанию false («ложь»).
2	
boolean m = false;
3	
if (a > b) 
    System.out.println(a);
 В переменную m будет записан результат сравнения – true или false. 
 Условие выполняется, если переданное в него выражение истинно – true.
4	
boolean m = (a > b);
if (m) 
    System.out.println(a);
5	
boolean m = (a > b);
if (m == true) 
    System.out.println(a);
Не нужно сравнивать логическую переменную (типа boolean) c true или false. 
Результат сравнения сам будет иметь тип boolean, и в точности будет совпадать со значением сравниваемой переменой: true == true – истина. 
Результат выражения – true. true == false – ложь. Результат сравнения – false.
6	
boolean m = (a > b);
if (m) 
    System.out.println(a);




package com.javarush.task.task04.task0426;

/* 
Ярлыки и числа
Ввести с клавиатуры целое число. Вывести на экран его строку-описание следующего вида:
«отрицательное четное число» — если число отрицательное и четное,
«отрицательное нечетное число» — если число отрицательное и нечетное,
«ноль» — если число равно 0,
«положительное четное число» — если число положительное и четное,
«положительное нечетное число» — если число положительное и нечетное.
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Integer num = Integer.parseInt(reader.readLine());
        
        String arg1;
        String arg2;
        if (num != 0) {
            if (num > 0) arg1 = "положительное";
            else arg1 = "отрицательное";

            if (num % 2 == 0) arg2 = "четное";
            else arg2 = "нечетное";

            System.out.println(arg1+" " + arg2+ " " + "число");
        }
        else System.out.println("ноль");
    }
}

package com.javarush.task.task04.task0427;

/* 
Описываем числа
Ввести с клавиатуры целое число в диапазоне 1 — 999. 
Вывести его строку-описание следующего вида:
«четное однозначное число» — если число четное и имеет одну цифру,
«нечетное однозначное число» — если число нечетное и имеет одну цифру,
«четное двузначное число» — если число четное и имеет две цифры,
«нечетное двузначное число» — если число нечетное и имеет две цифры,
«четное трехзначное число» — если число четное и имеет три цифры,
«нечетное трехзначное число» — если число нечетное и имеет три цифры.
Если введенное число не попадает в диапазон 1 — 999, 
в таком случае ничего не выводить на экран.
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        BufferedReader r = new BufferedReader(new InputStreamReader(System.in));
        String data = r.readLine();
        int num = Integer.parseInt(data);
        String arg1, arg2 = "0";

        if((num < 1) || (num > 999) ) return;
        else {
        if (num%2 == 0) arg1 = "четное";
        else arg1 = "нечетное";
        if (data.length() == 1 ) arg2 = "однозначное";
        if (data.length() == 2 ) arg2 = "двузначное";
        if (data.length() == 3 ) arg2 = "трехзначное";
        System.out.println(arg1 + " " + arg2 + " число");
        }
        
        /*
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Integer num = Integer.parseInt(reader.readLine());
        
        String arg1;
        String arg2;
        if (num != 0) {
            
            if (num % 2 == 0) arg1 = "четное";
            else arg1 = "нечетное";

            if (num <= 9) arg2 = "однозначное";
            else if (num <= 99) arg2 = "двузначное";
            else arg2 = "трехзначное";
            System.out.println(arg1+" " + arg2+ " " + "число");
        }
        */
        

    }
}

package com.javarush.task.task04.task0428;

/* 
Положительное число
Ввести с клавиатуры три целых числа. 
Вывести на экран количество положительных чисел в исходном наборе.
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Integer a = Integer.parseInt(reader.readLine());
        Integer b = Integer.parseInt(reader.readLine());
        Integer c = Integer.parseInt(reader.readLine());
        int count = 0;
        
        if(a>0) count++;
        if(b>0) count++;
        if(c>0) count++;
        
        System.out.println(count);
    }
}


package com.javarush.task.task04.task0429;

/* 
Положительные и отрицательные числа
Ввести с клавиатуры три целых числа. Вывести на экран количество 
положительных и количество отрицательных чисел в исходном наборе,
в следующем виде:
«количество отрицательных чисел: а», «количество положительных чисел: б»,
где а, б — искомые значения.
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Integer a = Integer.parseInt(reader.readLine());
        Integer b = Integer.parseInt(reader.readLine());
        Integer c = Integer.parseInt(reader.readLine());
        int countA = 0;
        int countB = 0;
        
        if(a>0) countA++; else if (a<0) countB++;
        if(b>0) countA++; else if (b<0) countB++;
        if(c>0) countA++; else if (c<0) countB++;
        
        System.out.println("количество отрицательных чисел: " + countB);
        System.out.println("количество положительных чисел: " + countA);

    }
}


package com.javarush.task.task04.task0432;
/* 
Хорошего много не бывает
Ввести с клавиатуры строку и число N.
Вывести на экран строку N раз используя цикл while. Каждое значение с новой строки.
*/
import java.io.*;
public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String text = reader.readLine();
        String n = reader.readLine();
        int chis = Integer.parseInt(n);
        int i = 1;
        while(i<=chis)
        {
            System.out.println(text);
            i++;
        }
        /*
        BufferedReader s = new BufferedReader(new InputStreamReader(System.in));
        String name = s.readLine();
        
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Integer age = Integer.parseInt(reader.readLine());
        
        int i=0;
        while(i<age){
            System.out.println(s);
            i++;
        }
        */
    }
}


package com.javarush.task.task04.task0433;
/* 
Гадание на долларовый счет
Вывести на экран квадрат из 10х10 букв S используя цикл while.
Буквы в каждой строке не разделять.
*/
import java.io.*;
public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        int i=0;
        while(i<10){
            int j=0;
            while(j<10){
                System.out.print("S");
                j++;
            }
            System.out.println();
            i++;
        }
    }
}


package com.javarush.task.task04.task0434;
/* 
Таблица умножения
*/
import java.io.*;
public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        int i=1;
        while(i<=10){
            int j=1;
            while(j<=10){
                System.out.print(i*j + " ");
                j++;
            }
            System.out.println();
            i++;
        }
    }
}

package com.javarush.task.task04.task0435;

/* 
Четные числа
Используя цикл for вывести на экран чётные числа от 1 до 100 включительно.
Каждое значение вывести с новой строки.
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        for(int i=2;i<=100;i=i+2){
            System.out.println(i);
        }

    }
}

package com.javarush.task.task04.task0436;


/* 
Рисуем прямоугольник
Ввести с клавиатуры два числа m и n.
Используя цикл for вывести на экран прямоугольник размером m на n из восьмёрок.
Пример: m=2, n=4
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Integer a = Integer.parseInt(reader.readLine());
        Integer b = Integer.parseInt(reader.readLine());
        
        for(int i=0;i<a;i++){
            for(int j=0;j<b;j++){
                System.out.print(8);
            }
            System.out.println();
        }
        

    }
}

package com.javarush.task.task04.task0437;


/* 
Треугольник из восьмерок
Используя цикл for вывести на экран прямоугольный треугольник из восьмёрок со сторонами 10 и 10.
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        for(int i=1;i<=10;i++){
            for(int j=0;j<i;j++){
                System.out.print(8);
            }
            System.out.println();
            
        }

    }
}

package com.javarush.task.task04.task0438;

/* 
Рисуем линии
Используя цикл for вывести на экран:
— горизонтальную линию из 10 восьмёрок
— вертикальную линию из 10 восьмёрок.
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        for(int i=0;i<10;i++)
            System.out.print(8);
        
        for(int j=0;j<10;j++)
            System.out.println(8);
    }
}


package com.javarush.task.task04.task0439;

/* 
Письмо счастья
Ввести с клавиатуры имя и используя цикл for 10 раз вывести: «*имя* любит меня.»

Пример вывода на экран для имени Света:
Света любит меня.
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String text = reader.readLine();
        for(int i=0;i<10;i++)
            System.out.println(text + " любит меня.");

    }
}

package com.javarush.task.task04.task0440;

/* 
Достойная оплата труда
Используя цикл вывести на экран сто раз надпись:
«Я никогда не буду работать за копейки. Амиго»
Каждое значение вывести с новой строки.
*/

public class Solution {
    public static void main(String[] args) {
        //напишите тут ваш код
        for(int i=0;i<100;i++)
            System.out.println("Я никогда не буду работать за копейки. Амиго");
    }
}

package com.javarush.task.task04.task0441;


/* 
Как-то средненько
Ввести с клавиатуры три числа, вывести на экран среднее из них.
Т.е. не самое большое и не самое маленькое.
Если все числа равны, вивести любое из них.
*/
import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Integer a = Integer.parseInt(reader.readLine());
        Integer b = Integer.parseInt(reader.readLine());
        Integer c = Integer.parseInt(reader.readLine());
        
        if ((a == b) && (a == c))
            System.out.println(a);
        else {
            if (a == b)
                System.out.println(a);
            if (a == c)
                System.out.println(a);
            if (b == c)
                System.out.println(b);
            if (((a > b) && (a < c)) || ((a < b) && (a > c)))
                System.out.println(a);
            if (((b < a) && (b > c)) || ((b > a) && (b < c)))
                System.out.println(b);
            if (((c > a) && (c < b)) || ((c < a) && (c > b)))
                System.out.println(c);
        }
        /*
        if((a>b && a<c) || (a<b && a>c)){
            System.out.println(a);
        }else if((b>a && b<c) || (b<a && b>c)){
            System.out.println(b);
        }else if((c>a && c<b) || (c<a && c>b)){
            System.out.println(c);
        }else if(a==b && b==c){
            System.out.println(a);
        }
        */
    }
}

package com.javarush.task.task04.task0442;


/* 
Суммирование
Вводить с клавиатуры числа и считать их сумму.
Если пользователь ввел -1, вывести на экран сумму и завершить программу.
-1 должно учитываться в сумме.
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        int s = 0;
        while (true){
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            int num = Integer.parseInt(reader.readLine());
            s += num;
            if (num == -1) {
                System.out.println(s);
                break;
            }
        }
    }
}

package com.javarush.task.task04.task0443;


/* 
Как назвали, так назвали
Ввести с клавиатуры строку name.
Ввести с клавиатуры дату рождения (три числа): y, m, d.

Вывести на экран текст:
«Меня зовут name.
Я родился d.m.y»

Пример вывода:
Меня зовут Вася.
Я родился 15.2.1988
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String name = reader.readLine();
        int y = Integer.parseInt(reader.readLine());
        int m = Integer.parseInt(reader.readLine());
        int d = Integer.parseInt(reader.readLine());
    
        System.out.println("Меня зовут "+name+"."+"\nЯ родился "+d+"."+m+"."+y);
        /*
        String text = reader.readLine();
        Integer a = Integer.parseInt(reader.readLine());
        Integer b = Integer.parseInt(reader.readLine());
        Integer c = Integer.parseInt(reader.readLine());
        
        System.out.println("Меня зовут " + text + ".");
        System.out.println("Я родился " + a + "." + "b" + "." + "c");
        */
    }
}

package com.javarush.task.task05.task0501;

/* 
Создание кота
Создать класс Cat. У кота должно быть имя (name, String), возраст (age, int), вес (weight, int), сила (strength, int).


Требования:
1.Программа не должна считывать данные с клавиатуры.
2.У класса Cat должна быть переменная name с типом String.
3.У класса Cat должна быть переменная age с типом int.
4.У класса Cat должна быть переменная weight с типом int.
5.У класса Cat должна быть переменная strength с типом int.
6.Всего в классе должно быть 4 переменных.
*/

public class Cat {
    //напишите тут ваш код
    public String name;
    public int age;
    public int weight;
    public int strength;

 public static void main(String[] args) {
    Cat catVaska = new Cat();
    
    }
}


package com.javarush.task.task05.task0502;

/* 
Реализовать метод fight
Реализовать метод boolean fight(Cat anotherCat):
реализовать механизм драки котов в зависимости от их веса, возраста и силы.
Зависимость придумать самому.
Метод должен определять, выиграли ли мы (this) бой или нет, 
т.е. возвращать true, если выиграли и false — если нет.

Должно выполняться условие:
если cat1.fight(cat2) = true,
то cat2.fight(cat1) = false


Требования:
1.Метод fight не должен считывать данные с клавиатуры.
2.Метод должен возвращать одно и тоже значение, если мы деремся с одним и тем же котом.
3.Если некий кот1 выигрывает у кота кот2, то кот2 должен проигрывать коту кот1.
4.Метод fight не должен выводить данные на экран.
*/

public class Cat {
    public String name;
    public int age;
    public int weight;
    public int strength;

    public Cat(){

    }

    public boolean fight(Cat anotherCat){
        return (
                (this.age > anotherCat.age) &&
                (this.weight > anotherCat.weight) &&
                (this.strength > anotherCat.strength)
        );
    }

    public static void main(String[] args) {
        Cat cat1 = new Cat();

        cat1.name = "Vaska";
        cat1.age = 8;
        cat1.weight = 8;
        cat1.strength = 8;

      Cat cat2 = new Cat();
        cat2.name = "Barsik";
        cat2.age = 7;
        cat2.weight = 7;
        cat2.strength = 7;

        cat1.fight(cat2);
        System.out.println(cat1.fight(cat2));

    }
}

package com.javarush.task.task05.task0503;


/* 
Геттеры и сеттеры для класса Dog
Создать class Dog. У собаки должна быть кличка String name и возраст int age.
Создайте геттеры и сеттеры для всех переменных класса Dog.


Требования:
1. Программа не должна считывать данные с клавиатуры.
2. У класса Dog должна быть переменная name с типом String.
3. У класса Dog должна быть переменная age с типом int.
4. У класса должен сеттер для переменной name.
5. У класса должен геттер для переменной name.
6. У класса должен сеттер для переменной age.
7. У класса должен геттер для переменной age.
*/

public class Dog {
    //напишите тут ваш код

    private String name;
    private int age;
    
    public String getName(){
        return name;
    }
    public void setName(String name){
        this.name=name;
    }
    
    public int getAge(){
        return age;
    }
    
    public void setAge(int age) {
        this.age=age;
    }


    public static void main(String[] args) {

    }
}


package com.javarush.task.task05.task0504;


/* 
Трикотаж
Трикотаж
Пару задач назад студенты секретного центра JavaRush создавали класс Cat. 
Теперь пришла пора реализовать котов во плоти, разумеется по образу 
и подобию класса Cat, а точнее — основываясь на нём, как на шаблоне. 
Их — котов — должно быть трое. 
Наполните этих троих жизнью, то есть, конкретными данными.


Требования:
1. Программа не должна считывать данные с клавиатуры.
2. Нужно создать три объекта типа Cat.
3. Класс Cat нельзя изменять.
4. Программа не должена выводить данные на экран.
*/

public class Solution {
    public static void main(String[] args) {
        //напишите тут ваш код
        Cat cat1 = new Cat("Barsik", 1, 3, 12);
        Cat cat2 = new Cat("Vasya", 2, 3, 14);
        Cat cat3 = new Cat("Mity", 3, 3, 13);
        
    }

    public static class Cat {
        private String name;
        private int age;
        private int weight;
        private int strength;

        public Cat(String name, int age, int weight, int strength) {
            this.name = name;
            this.age = age;
            this.weight = weight;
            this.strength = strength;
        }
    }
}

package com.javarush.task.task05.task0505;

/* 
Кошачья бойня
Создать три кота используя класс Cat.
Провести три боя попарно между котами.
Класс Cat создавать не надо. Для боя использовать метод boolean fight(Cat anotherCat).
Результат каждого боя вывести на экран c новой строки.


Требования:
1. Программа не должна считывать данные с клавиатуры.
2. Нужно создать три объекта типа Cat.
3. Нужно провести три боя.
4. Программа должна вывести результат каждого боя с новой строки.
*/

public class Solution {
    public static void main(String[] args) {
        //напишите тут ваш код
        Cat cat1 = new Cat("Barsik", 1, 3, 12);
        Cat cat2 = new Cat("Vasya", 2, 3, 14);
        Cat cat3 = new Cat("Mity", 3, 3, 13);
        
        cat1.fight(cat2);
        System.out.println(true);
        cat1.fight(cat3);
        System.out.println(true);
        cat2.fight(cat3);
        System.out.println(true); 
        
    }

    public static class Cat {
        protected String name;
        protected int age;
        protected int weight;
        protected int strength;

        public Cat(String name, int age, int weight, int strength) {
            this.name = name;
            this.age = age;
            this.weight = weight;
            this.strength = strength;
        }

        public boolean fight(Cat anotherCat) {
            int agePlus = this.age > anotherCat.age ? 1 : 0;
            int weightPlus = this.weight > anotherCat.weight ? 1 : 0;
            int strengthPlus = this.strength > anotherCat.strength ? 1 : 0;

            int score = agePlus + weightPlus + strengthPlus;
            return score > 2; // return score > 2 ? true : false;
        }
    }
}

package com.javarush.task.task05.task0506;

/* 
Человечки
Создать class Person. У человека должно быть имя String name, 
возраст int age, адрес String address, пол char sex.


Требования:
1. Программа не должна считывать данные с клавиатуры.
2. У класса Person должна быть переменная name с типом String.
3. У класса Person должна быть переменная age с типом int.
4. У класса Person должна быть переменная address с типом String.
5. У класса Person должна быть переменная sex с типом char.
6. Всего в классе должно быть 4 переменных.
*/

public class Person {
    //напишите тут ваш код
    String name;
    int age;
    String address;
    char sex;

    public static void main(String[] args) {

    }
}

package com.javarush.task.task05.task0508;

/* 
Геттеры и сеттеры для класса Person
Геттеры и сеттеры для класса Person
Создать class Person. У человека должно быть имя String name, возраст int age, пол char sex.
Создайте геттеры и сеттеры для всех переменных класса Person.


Требования:
1. Программа не должна считывать данные с клавиатуры.
2. У класса Person должна быть переменная name с типом String.
3. У класса Person должна быть переменная age с типом int.
4. У класса Person должна быть переменная sex с типом char.
5. У класса должен сеттер для переменной name.
6. У класса должен геттер для переменной name.
7. У класса должен сеттер для переменной age.
8. У класса должен геттер для переменной age.
9. У класса должен сеттер для переменной sex.
10. У класса должен геттер для переменной sex.
*/

public class Person {
    //напишите тут ваш код
    String name;
    int age;
    char sex;
    
    public void Person(String name, int age, char sex){
        this.name = name;
        this.age = age;
        this.sex = sex;
    }
    
    public void setName (String name){
        this.name = name;
    }
    public String getName(){
        return name;
    }
    
    public void setAge (int age){
        this.age = age;
    }
    public int getAge (){
        return age;
    }
    public void setSex(char sex){
        this.sex = sex;
    }
    public char getSex(){
        return sex;
    }

    public static void main(String[] args) {

    }
}


package com.javarush.task.task05.task0507;

/* 
Среднее арифметическое
Вводить с клавиатуры числа и вычислить среднее арифметическое.
Если пользователь ввел -1, вывести на экран среднее арифметическое 
всех чисел и завершить программу.
-1 не должно учитываться.

Пример для чисел 1 2 2 4 5 -1:
2.8

Пример для чисел 4 3 2 1 -1:
2.5


Требования:
1. Программа должна считывать данные с клавиатуры.
2. Программа должна выводить данные на экран.
3. Если ввести: 1 2 2 4 5 -1, программа должна вывести 2.8.
4. Если ввести: -100 0 100 -1, программа должна вывести 0.0.
5. Если ввести: 1 -1, программа должна вывести 1.0.
6. Выведенный результат должен соответствовать заданию для любых входных данных.
*/
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        BufferedReader b1 = new BufferedReader(new InputStreamReader(System.in));
        double i1 = Double.parseDouble(b1.readLine());
        double sum = 0;
        double srznach = 0;
        double kolichestvoCifr=0;
        while (i1!=-1) {
            sum = sum + i1;
            kolichestvoCifr=1+kolichestvoCifr;
            srznach = sum /kolichestvoCifr;
            i1 = Double.parseDouble(b1.readLine());
        }
        System.out.println(srznach);
    }
}



MyFile file = new MyFile();
file.initialize("c:\\data\\a.txt");

MyFile file2 = new MyFile();
file2.initialize(file, "a.txt");
String text = file2.readText();

class MyFile
{
    private String filename = null;

    public void initialize(String name)
    {
        this.filename = name;
    }
…
}

Пример с двумя методами initialize
class MyFile
{
    private String filename = null;
    public void initialize(String name)
    {
        this.filename = name;
    }

    public void initialize(String folder, String name)
    {
        this.filename = folder + name;
    }

…
}

Создание файла рядом с текущим файлом:
class MyFile
{
    private String filename = null;
    public void initialize(String name)
    {
        this.filename = name;
    }

    public void initialize(String folder, String name)
    {
        this.filename = folder + name;
    }

    // Файл filename будет находиться в той же директории, что и file.
    public void initialize(MyFile file, String name) 
    {       
        this.filename = file.getFolder() + name;
    }

…
}


Примеры
//Ex.1

MyFile file = new MyFile();
file.initialize("c:\data\a.txt");

String text = file.readText();

//Ex.2

MyFile file = new MyFile();
file.initialize("c:\data\", "a.txt");

String text = file.readText();

//Ex.3

MyFile file = new MyFile();
file.initialize("c:\data\a.txt");

MyFile file2 = new MyFile();
file2.initialize("a.txt");

String text = file2.readText();


package com.javarush.task.task05.task0509;

/* 
Создать класс Friend
Создать класс Friend
Создать класс Friend (друг) с тремя инициализаторами (тремя методами initialize):
— Имя
— Имя, возраст
— Имя, возраст, пол

Примечание:
Имя(String), возраст(int), пол(char).


Требования:
1. Программа не должна считывать данные с клавиатуры.
2. У класса Friend должна быть переменная name с типом String.
3. У класса Friend должна быть переменная age с типом int.
4. У класса Friend должна быть переменная sex с типом char.
5. У класса должен быть метод initialize, принимающий в качестве параметра имя и инициализирующий соответствующую переменную класса.
6. У класса должен быть метод initialize, принимающий в качестве параметров имя, возраст и инициализирующий соответствующие переменные класса.
7. У класса должен быть метод initialize, принимающий в качестве параметров имя, возраст, пол и инициализирующий соответствующие переменные класса.
*/

public class Friend {
    //напишите тут ваш код
    String name; 
    int age; 
    char sex;
    
    public void initialize(String name) { 
        this.name = name; 
    }
    
    public void initialize(String name, int age) { 
        this.name = name; 
        this.age = age; 
    }
    
    public void initialize(String name, int age, char sex) { 
        this.name = name; 
        this.age = age; 
        this.sex = sex; 
    }
    

    public static void main(String[] args) {

    }
}

package com.javarush.task.task05.task0510;

/* 
Кошкоинициация
Создать класс Cat (кот) с пятью инициализаторами:
— Имя,
— Имя, вес, возраст
— Имя, возраст (вес стандартный)
— вес, цвет (имя, адрес и возраст неизвестны, это бездомный кот)
— вес, цвет, адрес (чужой домашний кот)

Задача инициализатора – сделать объект валидным.
Например, если вес неизвестен, то нужно указать какой-нибудь средний вес.
Кот не может ничего не весить.
То же касательно возраста.
А вот имени может и не быть (null).
То же касается адреса: null.


Требования:
1. Программа не должна считывать данные с клавиатуры.
2. У класса Cat должна быть переменная name с типом String.
3. У класса Cat должна быть переменная age с типом int.
4. У класса Cat должна быть переменная weight с типом int.
5. У класса Cat должна быть переменная address с типом String.
6. У класса Cat должна быть переменная color с типом String.
7. У класса должен быть метод initialize, принимающий в качестве параметра имя, но инициализирующий все переменные класса, кроме адреса.
8. У класса должен быть метод initialize, принимающий в качестве параметров имя, вес, возраст и инициализирующий все переменные класса, кроме адреса.
9. У класса должен быть метод initialize, принимающий в качестве параметров имя, возраст и инициализирующий все переменные класса, кроме адреса.
10. У класса должен быть метод initialize, принимающий в качестве параметров вес, цвет и инициализирующий все переменные класса, кроме имени и адреса.
11. У класса должен быть метод initialize, принимающий в качестве параметров вес, цвет, адрес и инициализирующий все переменные класса, кроме имени.
*/

public class Cat {
    //напишите тут ваш код
    String name;
    int age;
    int weight;
    String address;
    String color;
    
//У класса должен быть метод initialize, принимающий в качестве параметра имя, 
//но инициализирующий все переменные класса, кроме адреса.
public void initialize(String name) { 
        this.name = name; 
        this.age = 3; 
        this.weight = 3;
        this.color = "white";
    }
// У класса должен быть метод initialize, принимающий в качестве параметров имя, 
//вес, возраст и инициализирующий все переменные класса, кроме адреса.
public void initialize(String name, int weight, int age) { 
        this.name = name; 
        this.age = age; 
        this.weight = weight;
        this.color = "white";
    }
// У класса должен быть метод initialize, принимающий в качестве параметров имя, 
//возраст и инициализирующий все переменные класса, кроме адреса.
public void initialize(String name, int age) { 
        this.name = name; 
        this.age = age; 
        this.weight = 3;
        this.color = "white";
    }

// У класса должен быть метод initialize, принимающий в качестве параметров вес, 
//цвет и инициализирующий все переменные класса, кроме имени и адреса.
public void initialize(int weight, String color) { 
        this.age = 3; 
        this.weight = weight;
        this.color = color;
    }

// У класса должен быть метод initialize, принимающий в качестве параметров вес, 
//цвет, адрес и инициализирующий все переменные класса, кроме имени.
public void initialize(int weight, String color, String address) { 
        this.age = 3; 
        this.weight = weight;
        this.color = color;
        this.address = address;
    }
  
    public static void main(String[] args) {

    }
}

package com.javarush.task.task05.task0511;

/* 
Создать класс Dog
Создать класс Dog (собака) с тремя инициализаторами:
— Имя
— Имя, рост
— Имя, рост, цвет


Требования:
1. Программа не должна считывать данные с клавиатуры.
2. У класса Dog должна быть переменная name с типом String.
3. У класса Dog должна быть переменная height с типом int.
4. У класса Dog должна быть переменная color с типом String.
5. У класса должен быть метод initialize, принимающий в качестве параметра имя 
//и инициализирующий соответствующую переменную класса.
6. У класса должен быть метод initialize, принимающий в качестве параметров имя, 
//рост и инициализирующий соответствующие переменные класса.
7. У класса должен быть метод initialize, принимающий в качестве параметров имя, 
//рост, цвет и инициализирующий соответствующие переменные класса.
*/

public class Dog {
    //напишите тут ваш код
    String name;
    int height;
    String color;
    
    public void initialize(String name) { 
        this.name = name; 
    }
    
    public void initialize(String name, int height) { 
        this.name = name;
        this.height = height;
    }
    
    public void initialize(String name, int height, String color) { 
        this.name = name;
        this.height = height;
        this.color = color;
    }

    public static void main(String[] args) {

    }
}

package com.javarush.task.task05.task0512;

/* 
Создать класс Circle
Создать класс (Circle) круг, с тремя инициализаторами:
— centerX, centerY, radius
— centerX, centerY, radius, width
— centerX, centerY, radius, width, color


Требования:
1. Программа не должна считывать данные с клавиатуры.
2. У класса Circle должны быть переменные centerX, centerY, radius, width и color с типом int.
3. У класса должен быть метод initialize, принимающий в качестве параметров
//centerX, centerY, radius и инициализирующий соответствующие переменные класса.
4. У класса должен быть метод initialize, принимающий в качестве параметров 
//centerX, centerY, radius, width и инициализирующий соответствующие переменные класса.
5. У класса должен быть метод initialize, принимающий в качестве параметров 
//centerX, centerY, radius, width, color и инициализирующий соответствующие переменные класса.
*/

public class Circle {
    //напишите тут ваш код
    private int centerX; 
    private int centerY; 
    private int radius; 
    private int width; 
    private int color;
    
    public void initialize (int centerX, int centerY, int radius){
        this.centerX = centerX;
        this.centerY = centerY;
        this.radius = radius;
    }
    
    public void initialize (int centerX, int centerY, int radius, int width){
        this.centerX = centerX;
        this.centerY = centerY;
        this.radius = radius;
        this.width = width;
    }
    
    public void initialize (int centerX, int centerY, int radius, int width, int color){
        this.centerX = centerX;
        this.centerY = centerY;
        this.radius = radius;
        this.width = width;
        this.color = color;
    }

    public static void main(String[] args) {

    }
}


package com.javarush.task.task05.task0513;

/* 
Собираем прямоугольник
Создать класс прямоугольник (Rectangle). Его данными будут top, left, width, height (верхняя координата, левая, ширина и высота).
Создать для него как можно больше методов initialize(…)

Примеры:
— заданы 4 параметра: left, top, width, height
— ширина/высота не задана (оба равны 0)
— высота не задана (равно ширине) создаём квадрат
— создаём копию другого прямоугольника (он и передаётся в параметрах)


Требования:
1. Программа не должна считывать данные с клавиатуры.
2. У класса Rectangle должны быть переменные top, left, width и height с типом int.
3. У класса должен быть хотя бы один метод initialize.
4. У класса должно быть хотя бы два метода initialize.
5. У класса должно быть хотя бы три метода initialize.
6. У класса должно быть хотя бы четыре метода initialize.

*/

public class Rectangle {
    //напишите тут ваш код
    private int left; 
    private int top; 
    private int width; 
    private int height;
    
    public void initialize (int left){
        this.left = left;
    }
    
    public void initialize (int left, int top){
        this.left = left;
        this.top = top;
    }
    public void initialize (int left, int top, int width){
        this.left = left;
        this.top = top;
        this.width = width;
    }
    public void initialize (int left, int top, int width, int height){
        this.left = left;
        this.top = top;
        this.width = width;
        this.height = height;
    }

    public static void main(String[] args) {

    }
}


package com.javarush.task.task05.task0514;

/* 
Программист создает человека
Создать class Person. У человека должно быть имя String name, возраст int age.
Добавь метод initialize(String name, int age), в котором проинициализируй переменные name и age.
В методе main создай объект Person, занеси его ссылку в переменную person.
Вызови метод initialize с любыми значениями.


Требования:
1. Программа не должна считывать данные с клавиатуры.
2. У класса Person должна быть переменная name с типом String.
3. У класса Person должна быть переменная age с типом int.
4. У класса должен быть метод initialize, принимающий в качестве параметра имя, возраст и инициализирующий соответствующие переменные класса.
5. Необходимо создать объект типа Person.
6. Необходимо вызвать метод initialize у созданного объекта и передать в него какие-либо параметры.
*/

public class Solution {
    public static void main(String[] args) {
        //напишите тут ваш код
        Person people = new Person();
        people.initialize("Sergey",36);
    }

    static class Person {
        //напишите тут ваш код
        String name;
        int age;
        
        public void initialize (String name, int age){
            this.name = name;
            this.age = age;
        }
        
    }
}

package com.javarush.task.task05.task0515;

/* 
Инициализация объектов
Изучи внимательно класс Person.
Исправь класс так, чтобы только один метод initialize инициализировал все переменные класса Person.


Требования:
1. Программа не должна считывать данные с клавиатуры.
2. У класса Person должно быть 5 переменных.
3. У класса должен быть один метод initialize.
4. Метод initialize должен иметь пять параметров.
*/

public class Person {
    String name;
    char sex;
    int money;
    int weight;
    double size;

    public void initialize(String name, char sex, int money, int weight, double size) {
        this.name = name;
        this.sex = sex;
        this.money = money;
        this.weight = weight;
        this.size = size;
    }

    public static void main(String[] args) {

    }
}



Без использования конструктора	С использованием конструктора
MyFile file = new MyFile();
file.initialize("c:\data\a.txt");
String text = file.readText();

MyFile file = new MyFile("c:\data\a.txt");
String text = file.readText();
**************************************************************
MyFile file = new MyFile();
file.initialize("c:\data\", "a.txt");
String text = file.readText();

MyFile file = new MyFile("c:\data\", "a.txt");
String text = file.readText();
*************************************************************
MyFile file = new MyFile();
file.initialize("c:\data\a.txt");

MyFile file2 = new MyFile();
file2.initialize( MyFile file, "a.txt");
String text = file2.readText();
************************************************************
MyFile file = new MyFile("c:\data\a.txt");


MyFile file2 = new MyFile(file, "a.txt");
String text = file2.readText();
***********************************************************



Без использования конструктора	
class MyFile
{
  private String filename = null;

  public void initialize(String name)
  {
    this.filename = name;
  }

  public void initialize(String folder, String name)
  {
    this.filename = folder + name;
  }

  public void initialize(MyFile file, String name)
  {
    this.filename = file.getFolder() + name;
  }

…
}

С использованием конструктора
class MyFile
{
  private String filename = null;

  public MyFile(String name)
  {
    this.filename = name;
  }

  public MyFile(String folder, String name)
  {
    this.filename = folder + name;
  }

  public MyFile(MyFile file, String name)
  {
    this.filename = file.getFolder() + name;
  }

…
}


package com.javarush.task.task05.task0516;

/* 
Друзей не купишь
Создать класс Friend (друг) с тремя конструкторами:
— Имя
— Имя, возраст
— Имя, возраст, пол

Требования:
1. У класса Friend должна быть переменная name с типом String.
2. У класса Friend должна быть переменная age с типом int.
3. У класса Friend должна быть переменная sex с типом char.
4. У класса должен быть конструктор, принимающий в качестве параметра имя
// и инициализирующий соответствующую переменную класса.
5. У класса должен быть конструктор, принимающий в качестве параметров имя, возраст 
// и инициализирующий соответствующие переменные класса.
6. У класса должен быть конструктор, принимающий в качестве параметров имя, возраст, пол 
// и инициализирующий соответствующие переменные класса.
*/

public class Friend {
    //напишите тут ваш код
    String name;
    int age;
    char sex;
    
    public Friend(String name){
        this.name = name;
    }
    
    public Friend(String name, int age){
        this.name = name;
        this.age = age;
    }
    
    public Friend(String name, int age, char sex){
        this.name = name;
        this.age = age;
        this.sex = sex;
    }

    public static void main(String[] args) {

    }
}


package com.javarush.task.task05.task0517;

/* 
Конструируем котиков
Создать класс Cat (кот) с пятью конструкторами:
— Имя,
— Имя, вес, возраст
— Имя, возраст (вес стандартный)
— вес, цвет, (имя, адрес и возраст – неизвестные. Кот — бездомный)
— вес, цвет, адрес (чужой домашний кот)

Задача конструктора – сделать объект валидным.
Например, если вес не известен, то нужно указать какой-нибудь средний вес.
Кот не может ничего не весить.
То же касательно возраста. А вот имени может и не быть (null). То же касается адреса: null.


Требования:
1. У класса Cat должна быть переменная name с типом String.
2. У класса Cat должна быть переменная age с типом int.
3. У класса Cat должна быть переменная weight с типом int.
4. У класса Cat должна быть переменная address с типом String.
5. У класса Cat должна быть переменная color с типом String.
6. У класса должен быть конструктор, принимающий в качестве параметра имя, 
 но инициализирующий все переменные класса, кроме адреса.
7. У класса должен быть конструктор, принимающий в качестве параметров имя, вес, возраст 
и инициализирующий все переменные класса, кроме адреса.
8. У класса должен быть конструктор, принимающий в качестве параметров имя, возраст и инициализирующий все переменные класса, кроме адреса.
9. У класса должен быть конструктор, принимающий в качестве параметров вес, цвет и инициализирующий все переменные класса, кроме имени и адреса.
10. У класса должен быть конструктор, принимающий в качестве параметров вес, цвет, адрес и инициализирующий все переменные класса, кроме имени.
*/

public class Cat {
    //напишите тут ваш код
    String name;
    int age;
    int weight;
    String address;
    String color;
    
    // 6. У класса должен быть конструктор, принимающий в качестве параметра имя, 
    // но инициализирующий все переменные класса, кроме адреса.
    public Cat(String name){
        this.name = name;
        this.age = 3;
        this.weight = 3;
        this.color = "white";
    }
    
    // 7. У класса должен быть конструктор, принимающий в качестве параметров 
    // имя, вес, возраст 
    // и инициализирующий все переменные класса, кроме адреса.
    public Cat(String name, int weight, int age){
        this.name = name;
        this.age = age;
        this.weight = weight;
        this.color = "white";
    }
    // 8. У класса должен быть конструктор, принимающий в качестве параметров 
    //имя, возраст и инициализирующий все переменные класса, кроме адреса.
    public Cat(String name, int age){
        this.name = name;
        this.age = age;
        this.weight = 3;
        this.color = "white";
    }
    
    // 9.У класса должен быть конструктор, принимающий в качестве параметров 
    //вес, цвет и инициализирующий все переменные класса, кроме имени и адреса.
    public Cat(int weight, String color){
        this.age = 3;
        this.weight = weight;
        this.color = color;
    }
    
    // 10. У класса должен быть конструктор, принимающий в качестве параметров 
    // вес, цвет, адрес и инициализирующий все переменные класса, кроме имени.
    public Cat(int weight, String color, String address){
        this.age = 3;
        this.weight = weight;
        this.color = color;
        this.address = address;
    }


    public static void main(String[] args) {

    }
}

package com.javarush.task.task05.task0518;

/* 
Регистрируем собачек
Создать класс Dog (собака) с тремя конструкторами:
— Имя
— Имя, рост
— Имя, рост, цвет


Требования:
1. У класса Dog должна быть переменная name с типом String.
2. У класса Dog должна быть переменная height с типом int.
3. У класса Dog должна быть переменная color с типом String.
4. У класса должен быть конструктор, принимающий в качестве параметра 
имя и инициализирующий соответствующую переменную класса.
5. У класса должен быть конструктор, принимающий в качестве параметров 
имя, рост и инициализирующий соответствующие переменные класса.
6. У класса должен быть конструктор, принимающий в качестве параметров 
имя, рост, цвет и инициализирующий соответствующие переменные класса.
*/


public class Dog {
    //напишите тут ваш код
    String name;
    int height;
    String color;
    
    public Dog(String name){
        this.name = name;
    }
    
    public Dog(String name, int height){
        this.name = name;
        this.height = height;
    }
    
    public Dog(String name, int height, String color){
        this.name = name;
        this.height = height;
        this.color = color;
    }

    public static void main(String[] args) {

    }
}

package com.javarush.task.task05.task0519;

/* 
Ходим по кругу
Создать класс (Circle) круг, с тремя конструкторами:
— centerX, centerY, radius
— centerX, centerY, radius, width
— centerX, centerY, radius, width, color


Требования:
1. У класса Circle должны быть переменные centerX, centerY, radius, width и color с типом int.
2. У класса должен быть конструктор, принимающий в качестве параметров 
centerX, centerY, radius и инициализирующий соответствующие переменные класса.
3. У класса должен быть конструктор, принимающий в качестве параметров 
centerX, centerY, radius, width и инициализирующий соответствующие переменные класса.
4. У класса должен быть конструктор, принимающий в качестве параметров 
centerX, centerY, radius, width, color и инициализирующий соответствующие переменные класса.
*/


public class Circle {
    //напишите тут ваш код
    int centerX; 
    int centerY; 
    int radius; 
    int width;
    int color;
    
    public Circle(int centerX, int centerY, int radius){
        this.centerX = centerX; 
        this.centerY = centerY; 
        this.radius = radius;
    }
    
    public Circle(int centerX, int centerY, int radius, int width){
        this.centerX = centerX; 
        this.centerY = centerY; 
        this.radius = radius;
        this.width = width;
    }
    
    public Circle(int centerX, int centerY, int radius, int width, int color){
        this.centerX = centerX; 
        this.centerY = centerY; 
        this.radius = radius;
        this.width = width;
        this.color = color;
    }

    public static void main(String[] args) {

    }
}

package com.javarush.task.task05.task0520;

/* 
Создать класс прямоугольник (Rectangle)

Создать класс прямоугольник (Rectangle). Его данными будут top, left, width, height 
(верхняя координата, левая, ширина и высота).
Создать для него как можно больше методов конструкторов.

Примеры:
— заданы 4 параметра: left, top, width, height
— ширина/высота не задана (оба равны 0)
— высота не задана (равно ширине) создаём квадрат
— создаём копию другого прямоугольника (он и передаётся в параметрах)


Требования:
1. У класса Rectangle должны быть переменные top, left, width и height с типом int.
2. У класса должен быть хотя бы один конструктор.
3. У класса должно быть хотя бы два конструктора.
4. У класса должно быть хотя бы три конструктора.
5. У класса должно быть хотя бы четыре конструктора.
*/


public class Rectangle {
    //напишите тут ваш код
    int top; 
    int left; 
    int width;
    int height;
    
    public Rectangle (int left){
        this.left = left; 
    }
    
    public Rectangle (int left, int top){
        this.left = left; 
        this.top = top; 
    }
    
    public Rectangle (int left, int top, int width){
        this.left = left; 
        this.top = top; 
        this.width = width; 
    }
    
    public Rectangle (int left, int top, int width, int height){
        this.left = left; 
        this.top = top; 
        this.width = width; 
        this.height = height;
    }
    
    public static void main(String[] args) {

    }
}

package com.javarush.task.task05.task0521;

/* 
Вызов конструктора из конструктора
Разберись, что делает программа.
Исправь конструктор с двумя параметрами так, 
чтобы он вызывал другой конструктор с радиусом 10.
Подумай, какой конструктор нужно вызвать.
Подсказка:
внимательно изучи реализацию конструктора по умолчанию.


Требования:
1. У класса должно быть 3 конструктора.
2. Конструктор с двумя параметрами должен инициализировать переменные x и y переданными значениями.
3. Конструктор с двумя параметрами должен инициализировать переменную radius значением 10.
4. Конструктор с двумя параметрами должен должен вызывать другой конструктор, передав в него верные значения параметров.
5. Метод main не изменять.
6. Конструктор по умолчанию не изменять.
*/

public class Circle {

    public double x;
    public double y;
    public double radius;

    public Circle(double x, double y, double radius) {
        this.x = x;
        this.y = y;
        this.radius = radius;
    }

    public Circle(double x, double y) {
        //напишите тут ваш код
        this(x,y,10);
    }

    public Circle() {
        this(5, 5, 1);
    }

    public static void main(String[] args) {
        Circle circle = new Circle();
        System.out.println(circle.x + " " + circle.y + " " + circle.radius);
        Circle anotherCircle = new Circle(10, 5);
        System.out.println(anotherCircle.x + " " + anotherCircle.y + " " + anotherCircle.radius);
    }
}

package com.javarush.task.task05.task0522;

/* 
Максимум конструкторов

Изучи класс Circle. Напиши максимальное количество конструкторов с разными аргументами.

Подсказка:
не забудь про конструктор по умолчанию.


Требования:
1. У класса должно быть хотя бы три переменные.
2. У класса должен быть конструктор по умолчанию.
3. У класса должен быть хотя бы один конструктор.
4. У класса должно быть хотя бы два конструктора.
5. У класса должно быть хотя бы три конструктора.
6. У класса должно быть хотя бы четыре конструктора.
*/

public class Circle {
    public double x;
    public double y;
    public double radius;

    //напишите тут ваш код
    public Circle(){
        
    }

    public Circle(double x){
        this.x = x;
    }
    
    public Circle(double x, double y){
        this.x = x;
        this.y = y;
    }
    
    public Circle(double x, double y, double radius){
        this.x = x;
        this.y = y;
        this.radius = radius;
    }
        
    public static void main(String[] args) {

    }
}

package com.javarush.task.task05.task0523;

/* 
Конструктор

Разберись, что делает программа.
Найди и исправь одну ошибку в классе Circle. Метод main изменять нельзя.

Подсказка:
изучи конструктор по умолчанию.


Требования:
1. Программа не должна считывать данные с клавиатуры.
2. Метод main изменять нельзя.
3. Метод setDescription класса Color должен устанавливать значение переменной description.
4. Метод getDescription класса Color должен возвращать значение переменной description.
5. Программа должна вывести слово "Red" на экран.
*/

public class Circle {
    public Color color;

    public static void main(String[] args) {
        Circle circle = new Circle();
        circle.color.setDescription("Red");
        System.out.println(circle.color.getDescription());
    }

    public void Circle() {
        color = new Color();
    }

    public class Color {
        private String description;

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }
}

package com.javarush.task.task05.task0524;

/* 
Основа колеса
В классе Circle создать конструктор который проинициализирует все переменные класса.
В конструкторе должно быть три аргумента.


Требования:
1. У класса Circle должны быть переменные x, y и r с типом double.
2. У класса должен быть один конструктор.
3. Конструктор должен быть public.
4. Конструктор должен иметь три параметра типа double.
5. Конструктор должен проинициализировать все переменные класса.

*/

public class Circle {
    public double x;
    public double y;
    public double r;

    //напишите тут ваш код
    public Circle (double x, double y, double r){
        this.x = x;
        this.y = y;
        this.r = r;
    }
    
    public static void main(String[] args) {

    }
}



package com.javarush.task.task05.task0525;

/* 
И целой утки мало
По аналогии с классом Duck (утка) создай классы Cat (кошка) и Dog (собака).
Подумай, что должен возвращать метод toString в классах Cat и Dog?
В методе main создай по два объекта каждого класса и выведи их на экран.
Объекты класса Duck уже созданы и выводятся на экран.


Требования:
1. Создай класс Cat(кошка).
2. Создай класс Dog(собака).
3. У класса Cat должен быть верно реализован метод toString.
4. У класса Dog должен быть верно реализован метод toString.
5. В методе main создай два объекта типа Cat.
6. В методе main создай два объекта типа Dog.
7. Выведи все созданные объекты на экран.
*/

public class Solution {

    public static void main(String[] args) {
        Duck duck1 = new Duck();
        Duck duck2 = new Duck();
        System.out.println(duck1);
        System.out.println(duck2);

        //напишите тут ваш код
        Cat cat1 = new Cat();
        Cat cat2 = new Cat();
        System.out.println(cat1);
        System.out.println(cat2);
        
        Dog dog1 = new Dog();
        Dog dog2 = new Dog();
        System.out.println(dog1);
        System.out.println(dog2);
    }

    public static class Duck {
        public String toString() {
            return "Duck";
        }
    }

    //напишите тут ваш код
    public static class Cat {
        public String toString() {
            return "Cat";
        }
    }
    
    public static class Dog {
        public String toString() {
            return "Dog";
        }
    }
}


package com.javarush.task.task05.task0526;

/* 
Мужчина и женщина
1. Внутри класса Solution создай public static классы Man и Woman.
2. У классов должны быть поля: name (String), age (int), address (String).
3. Создай конструкторы, в которые передаются все возможные параметры.
4. Создай по два объекта каждого класса со всеми данными используя конструктор.
5. Объекты выведи на экран в таком формате «name + » » + age + » « + address».


Требования:
1. В классе Solution создай public static класс Man.
2. В классе Solution создай public static класс Woman.
3. Класс Man должен содержать переменные: name(String), age(int) и address(String).
4. Класс Woman должен содержать переменные: name(String), age(int) и address(String).
5. У классов Man и Woman должны быть конструкторы, принимающие параметры с типами String, int и String.
6. Конструкторы должны инициализировать переменные класса.
7. В методе main необходимо создать по два объекта каждого типа.
8. Метод main должен выводить созданные объекты на экран в указанном формате.

*/

public class Solution {
    public static void main(String[] args) {
        //напишите тут ваш код
        Man man1 = new Man("Sergey", 35, "Russia");
        Man man2 = new Man("Alex", 30, "USA");
        System.out.println(man1.name + " " + man1.age + " " + man1.address);
        System.out.println(man2.name + " " + man2.age + " " + man2.address);
        
        Woman woman1 = new Woman("Lena", 35, "Russia");
        Woman woman2 = new Woman("Alex", 30, "USA");
        System.out.println(woman1.name + " " + woman1.age + " " + woman1.address);
        System.out.println(woman2.name + " " + woman2.age + " " + woman2.address);
        
    }

    //напишите тут ваш код
    public static class Man{
        String name;
        int age;
        String address;
        
        public Man (String name, int age, String address){
            this.name = name;
            this.age = age;
            this.address = address;
        }
    }
    
    public static class Woman{
        String name;
        int age;
        String address;
        
        public Woman (String name, int age, String address){
            this.name = name;
            this.age = age;
            this.address = address;
        }
    }
}


package com.javarush.task.task05.task0527;

/* 
Том и Джерри
Создай классы Dog, Cat, Mouse.
Добавь по три поля в каждый класс, на твой выбор.
Создай объекты для героев мультика Том и Джерри.
Так много, как только вспомнишь.

Пример:
Mouse jerryMouse = new Mouse(“Jerry”, 12 , 5),
где 12 — высота в см,
5 — длина хвоста в см.


Требования:
1. Создай класс Dog.
2. Создай класс Cat.
3. В классе Dog должно быть три переменные.
4. В классе Cat должно быть три переменные.
5. Должен быть создан хотя бы один объект типа Mouse.
6. Должен быть создан хотя бы один объект типа Dog.
7. Должен быть создан хотя бы один объект типа Cat.
*/

public class Solution {
    public static void main(String[] args) {
        Mouse jerryMouse = new Mouse("Jerry", 12, 5);
        Dog jerryDog = new Dog("Jerry", 12, 5);
        Cat jerryCat = new Cat("Jerry", 12, 5);

        //напишите тут ваш код
    }

    public static class Mouse {
        String name;
        int height;
        int tail;

        public Mouse(String name, int height, int tail) {
            this.name = name;
            this.height = height;
            this.tail = tail;
        }
    }

    //напишите тут ваш код
    public static class Dog {
        String name;
        int height;
        int tail;

        public Dog(String name, int height, int tail) {
            this.name = name;
            this.height = height;
            this.tail = tail;
        }
    }
    
    public static class Cat {
        String name;
        int height;
        int tail;

        public Cat(String name, int height, int tail) {
            this.name = name;
            this.height = height;
            this.tail = tail;
        }
    }
}


package com.javarush.task.task05.task0528;

/* 
Вывести на экран сегодняшнюю дату
Вывести на экран текущую дату в аналогичном виде «21 02 2014«.


Требования:
1. Дата должна содержать день, месяц и год, разделенные пробелом.
2. День должен соответствовать текущему.
3. Месяц должен соответствовать текущему.
4. Год должен соответствовать текущему.
5. Вся дата должна быть выведена в одной строке.
*/

import java.util.Date;

public class Solution {
    public static void main(String[] args) {
        //напишите тут ваш код
        System.out.println(String.format("%1$td %1$tm %1$tY", new Date()));  
    }
}



import java.text.SimpleDateFormat;
import java.util.Date;
public class Test {
    public void test()
    {
        Date d = new Date();
        SimpleDateFormat format1 = new SimpleDateFormat("dd.MM.yyyy hh:mm");
        SimpleDateFormat format2 = new SimpleDateFormat("День dd Месяц MM Год yyyy Время hh:mm");
        System.out.println(format1.format(d)); //25.02.2013 09:03
        System.out.println(format2.format(d)); //День 25 Месяц 02 Год 2013 Время 09:03
    }
}



package com.javarush.task.task05.task0529;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/* 
Консоль-копилка
Вводить с клавиатуры числа и считать их сумму, пока пользователь не введет слово «сумма».
Вывести на экран полученную сумму.


Требования:
1. Программа должна считывать данные с клавиатуры.
2. Программа должна корректно работать, если пользователь ввел одно число и слово «сумма».
3. Программа должна корректно работать, если пользователь ввел два числа число и слово «сумма».
4. Программа должна корректно работать, если пользователь ввел более двух чисел и слово «сумма».
5. Программа должна выводить текст на экран.
6. Программа должна прекращать считывать данные с клавиатуры после того пользователь 
введет слово "сумма" и нажмет Enter.

*/

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
                BufferedReader r = new BufferedReader(new InputStreamReader(System.in));
        int sum = 0;
        while (true) {
            String s1 = r.readLine();
            if (s1.equals("сумма")) {
                System.out.println(sum);
                break;
            } else {
                int a1 = Integer.parseInt(s1);
                sum += a1;
            }
        }
        
    }
}


public class MainClass
{
   public static void main (String[] args)
   {
    ┏ Tommy
    ┃ Cat cat = new Cat("Tommy");
    ┃ cat = null;
    ┗
	
	
    ┏ Sammy
	┃ Cat cat1 = new Cat("Sammy");
		┃┏ Maisy
			┃┃Cat cat2 = new Cat("Maisy");
			┃┃cat2 = cat1;
		┃┗
    
		┃┏ Ginger
			┃┃cat1 = new Cat("Ginger");
		┃┃ cat2 = null;
    ┃┗
    ┗
   }
}

Пример:
class Cat
{
    String name;

    Cat(String name)
    {
        this.name = name;
    }

    protected void finalize() throws Throwable
    {
        System.out.println(name + " destroyed");
    }
}

package com.javarush.task.task06.task0601;

/* 
Метод finalize класса Cat
В классе Cat создать метод protected void finalize() throws Throwable.


Требования:
1. В классе Cat должен быть метод с именем finalize.
2. Метод finalize ничего не должен возвращать (void).
3. Метод finalize должен быть protected.
4. Метод finalize может кидать исключение Throwable, укажи это при объявлении метода.

*/

public class Cat {
    //напишите тут ваш код
    protected void finalize() throws Throwable
    {
        
    }
    
    public static void main(String[] args) {

    }
}

package com.javarush.task.task06.task0602;

/* 
Пустые кошки, пустые псы
В каждом классе Cat и Dog написать метод finalize, который выводит на экран текст о том, что такой-то объект уничтожен.


Требования:
1. В классе Cat должен быть void метод finalize.
2. В классе Dog должен быть void метод finalize.
3. Метод finalize класса Cat должен выводить на экран "Cat was destroyed".
4. Метод finalize класса Dog должен выводить на экран "Dog was destroyed".
*/

public class Cat {
    public static void main(String[] args) {

    }

    //напишите тут ваш код
    protected void finalize() throws Throwable
    {
        System.out.println("Cat was destroyed");
    }

}

class Dog {
    //напишите тут ваш код
    protected void finalize() throws Throwable
    {
        System.out.println("Dog was destroyed");
    }
}

package com.javarush.task.task06.task0603;

/* 
По 50 000 объектов Cat и Dog
Создать в цикле по 50 000 объектов Cat и Dog.
(Java-машина должна начать уничтожать неиспользуемые, и метод finalize хоть раз да вызовется).


Требования:
1. В классе Cat должен быть void метод finalize.
2. В классе Dog должен быть void метод finalize.
3. Метод finalize класса Cat должен выводить на экран "Cat was destroyed".
4. Метод finalize класса Dog должен выводить на экран "Dog was destroyed".
5. Метод main должен создавать 50000 объектов типа Cat и 50000 объектов типа Dog.
*/

public class Solution {
    public static void main(String[] args) {
        //напишите тут ваш код
        for (int i=0; i<50000; i++) {
            Cat cat=new Cat();
            Dog dog=new Dog();
        }
    }
}

class Cat {
    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        System.out.println("Cat was destroyed");
    }
}

class Dog {
    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        System.out.println("Dog was destroyed");
    }
}

package com.javarush.task.task06.task0604;

/* 
Ставим котов на счётчик
В конструкторе класса Cat [public Cat()] увеличивать счётчик котов (статическую переменную этого же класса catCount) на 1. В методе finalize уменьшать на 1.


Требования:
1. Добавь в класс Cat конструктор без параметров public Cat().
2. Конструктор класса должен на 1 увеличивать значение переменной catCount.
3. Добавь в класс Cat метод finalize.
4. Метод finalize ничего не должен возвращать (тип возвращаемого значения void).
5. Метод finalize должен уменьшать переменную catCount на 1.
*/

public class Cat {
    public static int catCount = 0;
    //напишите тут ваш код
    
    public Cat (){
        this.catCount ++;
        
    }

    protected void finalize() throws Throwable {
        this.catCount --;
    }

    public static void main(String[] args) {

    }
}

package com.javarush.task.task06.task0606;

import java.io.*;

/* 
Чётные и нечётные циферки
Ввести с клавиатуры число. Определить, сколько в введенном числе четных цифр, 
а сколько нечетных.
Если число делится без остатка на 2 (т. е. остаток равен нулю), значит оно четное.
Увеличиваем на 1 счетчик четных цифр (статическая переменная even).
Иначе число нечетное, увеличиваем счетчик нечетных цифр (статическая переменная odd).
Вывести на экран сообщение: «Even: а Odd: b», где а — количество четных цифр, 
b — количество нечетных цифр.

Пример для числа 4445:
Even: 3 Odd: 1


Требования:
1. Программа должна считывать данные с клавиатуры.
2. Метод main должен посчитать сколько четных цифр во веденном числе и записать 
это количество в переменную even.
3. Метод main должен посчитать сколько нечетных цифр во веденном числе и записать 
это количество в переменную odd.
4. Программа должна выводить текст на экран.
5. Выведенный текст должен соответствовать заданию.
*/

public class Solution {

    public static int even;
    public static int odd;

    public static void main(String[] args) throws IOException {
        //напишите тут ваш код
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int x = Integer.parseInt(reader.readLine());
        int y;
        
        while (x>0){
            y = x%10;
            if(y%2 == 0) even++;
            else odd++;
            x = x / 10; 
        }
        System.out.println("Even: "+ even + " Odd: " + odd);
    }
}


Объявление класса
class Cat                        //класс
{
    String name;                 //переменная  

    Cat(String name)             //конструктор 
    {
        this.name = name;        //инициализация переменной 
    }
}

Код в методе main:

	Cat cat1 = new Cat("Vaska"); //создали один объект, его name содержит строку «Vaska»
	Cat cat2 = new Cat("Murka"); //создали один объект, его name содержит строку «Murka»

	System.out.println(cat1.name);
	System.out.println(cat2.name);

Вывод на экран будет таким:

	Vaska
	Murka

— Переменные cat1.name и cat2.name, хоть и описаны в одном классе – Cat, но хранят разные значения, т.к. привязаны к разным объектам.
— Это понятно.
— Статические же переменные – существуют в одном экземпляре, и обращаться к ним нужно по имени класса:

Объявление класса
class Cat                   //класс
{
    String name;            //обычная переменная
    static int catCount;    //статическая переменная

    Cat(String name)
    {
        this.name = name;
        Cat.catCount++;   //увеличиваем значение статический переменной на 1
    }
}

Код в методе main:
	System.out.println(Cat.catCount);
	Cat cat1 = new Cat("Vaska");

	System.out.println(Cat.catCount);
	Cat cat2 = new Cat("Murka");

	System.out.println(cat1.name);
	System.out.println(cat2.name);
	System.out.println(Cat.catCount);

Вывод на экран будет таким:

0
1
Vaska
Murka
2


— Это тоже понятно.

— Методы класса тоже делятся на две категории. Обычные методы вызываются у объекта и имеют доступ к данным этого объекта. Статические методы не имеют такого доступа – у них просто нет ссылки на объект, они способны обращаться либо к статическим переменным этого класса либо к другим статическим методам.

— Статические методы не могут обращаться к нестатическим методам или нестатическим переменным!

— А почему?

— Каждая обычная переменная класса находится внутри объекта. Обратиться к ней можно только имея ссылку на этот объект. В статический метод такая ссылка не передается.

— А в обычные методы передается?

— Да, в обычные методы передается, неявно. В каждый метод неявно передается ссылка на объект, у которого этот метод вызывают. Переменная, которая хранит эту ссылку, называется this. Таким образом, метод всегда может получить данные из своего объекта или вызвать другой нестатический метод этого же объекта.

— В статический метод вместо ссылки на объект передается null. Поэтому он не может обращаться к нестатическим переменным и методам – у него банально нет ссылки на объект, к которому они привязаны.

— Ясно.

— Так работают обычные нестатические методы:

Как выглядит код
Cat cat = new Cat();
String name = cat.getName();
cat.setAge(17);
cat.setChildren(cat1, cat2, cat3);
Что происходит на самом деле
Cat cat = new Cat();
String name = Cat.getName(cat);
Cat.setAge(cat, 17);
Cat.setChildren(cat, cat1, cat2, cat3);
При вызове метода в виде «объект» точка «имя метода», на самом деле вызывается метод класса, в который первым аргументом передаётся тот самый объект. Внутри метода он получает имя this. Именно с ним и его данными происходят все действия.
— А вот как работают статические методы:

Как выглядит код
Cat cat1 = new Cat();
Cat cat2 = new Cat();
int catCount = Cat.getAllCatsCount();
Что происходит на самом деле
Cat cat1 = new Cat();
Cat cat2 = new Cat();
int catCount = Cat.getAllCatsCount(null);
При вызове статического метода, никакого объекта внутрь не передаётся. Т.е. this равен null, поэтому статический метод не имеет доступа к нестатическим переменным и методам (ему нечего неявно передать в обычные методы).
— Переменная или метод являются статическими, если перед ними стоит ключевое слово static.

— А зачем такие методы нужны, если они так сильно ограничены?

— У такого подхода тоже есть свои преимущества.

— Во-первых, для того, чтобы обратиться к статическим методам и переменным не надо передавать никакую ссылку на объект.

— Во-вторых, иногда бывает нужно, чтобы переменная была в единственном экземпляре. Как, например, переменная System.out (статическая переменная out класса System).

— И в третьих, иногда нужно вызвать метод, еще до того, как будет возможность создавать какие-то объекты

— Это когда же?

— А почему, по-твоему, метод main объявлен статическим? Чтобы его можно было вызвать сразу после загрузки класса в память. Еще до того, когда можно будет создавать какие-то объекты.



public class StaticClassExample
{
    private static int catCount = 0;

    public static void main(String[] args) throws Exception
    {
        Cat vaska = new Cat("Bella");
        Cat murka = new Cat("Tiger");

        System.out.println("Cat count " + catCount);
    }
    
    public static class Cat
    {
        private String name;

        public Cat(String name)
        {
            this.name = name;
            StaticClassExample.catCount++;
        }
    }

}

— Объектов класса Cat можно создавать сколько угодно. В отличие от, например, статической переменной, которая существует в единственном экземпляре.

— Основной смысл модификатора static перед объявлением класса — это регулирование отношения класса Cat к классу StaticClassExample. Смысл примерно такой: класс Cat не привязан к объектам класса StaticClassExample, и не может обращаться к обычным (нестатическим) переменным класса StaticClassExample.

— Значит, я могу создавать классы внутри других классов?

— Да. Java такое позволяет. Не слишком задумывайся об этом сейчас. В будущем я объясню еще некоторые вещи, и все станет немного проще.

— Надеюсь.


package com.javarush.task.task06.task0607;

/* 
Классовый счетчик
Создать статическую переменную int catCount в классе Cat. 
Создай конструктор [public Cat()], в котором увеличивай данную переменную на 1.


Требования:
1. Добавь в класс Cat конструктор.
2. Конструктор должен быть public.
3. Добавь в класс Cat поле catCount.
4. Поле catCount должно быть типа int.
5. Поле catCount должно быть статическим.
6. Конструктор класса должен на 1 увеличивать значение переменной catCount.
*/

public class Cat {
    //напишите тут ваш код
    static int catCount;
    
    public Cat(){
        Cat.catCount++; 
    }
    public static void main(String[] args) {

    }
}

package com.javarush.task.task06.task0608;

/* 
Статические методы для кошек
Добавить к классу Cat два статических метода: int getCatCount() и setCatCount(int),
с помощью которых можно получить/изменить количество котов (переменную catCount).


Требования:
1. Добавь в класс метод getCatCount.
2. Метод getCatCount должен возвращать int.
3. Метод getCatCount должен возвращать значение переменной catCount.
4. Добавь в класс метод setCatCount, принимающий int.
5. Метод setCatCount ничего не должен возвращать.
6. Метод setCatCount должен присваивать переменной catCount переданное значение.
*/

public class Cat {
    private static int catCount = 0;

    public Cat() {
        catCount++;
    }

    public static int getCatCount() {
        //напишите тут ваш код
        return Cat.catCount;

    }

    public static void setCatCount(int catCount) {
        //напишите тут ваш код
        Cat.catCount = catCount;

    }

    public static void main(String[] args) {

    }
}

package com.javarush.task.task06.task0609;

/* 
Расстояние между двумя точками
Реализовать статический метод double getDistance(x1, y1, x2, y2). 
Он должен вычислять расстояние между точками.
Используй метод double Math.sqrt(double a), который вычисляет квадратный 
корень переданного параметра.


Требования:
1. Метод getDistance должен возвращать double.
2. Метод getDistance должен быть статическим.
3. Метод getDistance должен быть public.
4. Метод getDistance должен возвращать расстояние между точками.
5. Метод getDistance должен использовать метод double Math.sqrt(double a).
*/

public class Util {
    public static double getDistance(int x1, int y1, int x2, int y2) {
        //напишите тут ваш код
         return (Math.sqrt(Math.pow((x2-x1),2)+ Math.pow((y2-y1),2)));
    }

    public static void main(String[] args) {

    }
}


package com.javarush.task.task06.task0610;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/* 
Класс ConsoleReader
Сделать класс ConsoleReader, у которого будут 4 статических метода:
String readString() – читает с клавиатуры строку
int readInt() – читает с клавиатуры число
double readDouble() – читает с клавиатуры дробное число
boolean readBoolean() – читает с клавиатуры строку 
«true» или «false» и возвращает соответствующую логическую переменную true или false

Внимание: создавайте переменную для чтения данных с консоли 
(BufferedReader или Scanner) внутри каждого метода.


Требования:
1. Метод readString должен считывать и возвращать строку(тип String).
2. Метод readInt должен считывать и возвращать число(тип int).
3. Метод readDouble должен считывать и возвращать дробное число(тип double).
4. Метод readBoolean должен считывать и возвращать логическую переменную(тип boolean).
*/

public class ConsoleReader {
    public static String readString() throws Exception {
        //напишите тут ваш код
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String s = reader.readLine();
        return s;
    }

    public static int readInt() throws Exception {
        //напишите тут ваш код
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int i = Integer.parseInt(reader.readLine());
        return i;
    }

    public static double readDouble() throws Exception {
        //напишите тут ваш код
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Double i = Double.parseDouble(reader.readLine());
        return i;
    }

    public static boolean readBoolean() throws Exception {
        //напишите тут ваш код
        //BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        return Boolean.parseBoolean(ConsoleReader.readString());

    }

    public static void main(String[] args) {

    }
}


/* 
Класс ConsoleReader
*/

public class ConsoleReader {
    public static String readString() throws Exception {
        //напишите тут ваш код
        return new BufferedReader(new InputStreamReader(System.in)).readLine();


    }

    public static int readInt() throws Exception {
        //напишите тут ваш код
        return Integer.parseInt(ConsoleReader.readString());

    }

    public static double readDouble() throws Exception {
        //напишите тут ваш код
        return Double.parseDouble(ConsoleReader.readString());

    }

    public static boolean readBoolean() throws Exception {
        //напишите тут ваш код
       return Boolean.parseBoolean(ConsoleReader.readString());

    }

    public static void main(String[] args) {

    }
}

package com.javarush.task.task06.task0611;

/* 
Класс StringHelper
Сделать класс StringHelper, у которого будут 2 статических метода:
String multiply(String s, int count) – возвращает строку повторенную count раз.
String multiply(String s) – возвращает строку повторенную 5 раз.

Пример:
Амиго -> АмигоАмигоАмигоАмигоАмиго


Требования:
1. Методы класса StringHelper должны возвращать строку.
2. Методы класса StringHelper должны быть статическими.
3. Методы класса StringHelper должны быть public.
4. Метод multiply(String s, int count) должен возвращать строку повторенную count раз.
5. Метод multiply(String s) должен возвращать строку повторенную 5 раз.
*/

public class StringHelper {
    public static String multiply(String s) {
        String result = "";
        //напишите тут ваш код
        for(int i=0; i<5; i++)
            result +=s;
        
        return result;
    }

    public static String multiply(String s, int count) {
        String result = "";
        //напишите тут ваш код
        for(int i=0; i<count; i++)
            result +=s;
        
        return result;
    }

    public static void main(String[] args) {

    }
}

package com.javarush.task.task06.task0605;


import java.io.*;

/* 
Контролируем массу тела
Программа должна считывать введенные пользователем вес в килограммах и 
рост в метрах и выводить на экран сообщение о индексе массы тела.
Реализуй статический метод класса Body. Метод должен определить 
индекс массы тела, и вывести на экран сообщение:
«Недовес: меньше чем 18.5» — если индекс массы тела меньше 18.5,
«Нормальный: между 18.5 и 24.9» — если индекс массы тела между 18.5 и 24.9,
«Избыточный вес: между 25 и 29.9» — если индекс массы тела между 25 и 29.9,
«Ожирение: 30 или больше» — если индекс массы тела 30 или больше.

Подсказка:
Индекс массы тела = вес в кг / (рост в метрах * рост в метрах)

Пример вывода для 68.4 и 1.77:
Нормальный: между 18.5 и 24.9


Требования:
1. Метод massIndex должен выводить текст на экран.
2. Метод massIndex должен выводить "Недовес: меньше чем 18.5" на экран, 
если индекс массы тела меньше 18.5.
3. Метод massIndex должен выводить "Нормальный: между 18.5 и 24.9" на экран, 
если индекс массы тела между 18.5 и 24.9.
4. Метод massIndex должен выводить "Избыточный вес: между 25 и 29.9" на экран, 
если индекс массы тела между 25 и 29.9.
5. Метод massIndex должен выводить "Ожирение: 30 или больше" на экран, 
если индекс массы тела 30 или больше.
*/

public class Solution {

    public static void main(String[] args) throws IOException {
        BufferedReader bis = new BufferedReader(new InputStreamReader(System.in));
        double weight = Double.parseDouble(bis.readLine());
        double height = Double.parseDouble(bis.readLine());

        Body.massIndex(weight, height);
    }

    public static class Body {
        public static void massIndex(double weight, double height) {
            //напишите тут ваш код
            double index = weight/(height*height);
            if (index < 18.5) System.out.println("Недовес: меньше чем 18.5");
            else if ((index >= 18.5) && (index <= 24.9)) 
            System.out.println("Нормальный: между 18.5 и 24.9");
            else if ((index >= 25) && (index <= 29.9))
            System.out.println("Избыточный вес: между 25 и 29.9");
            else if (index >= 30)
            System.out.println("Ожирение: 30 или больше");
            
        }
    }
}

package com.javarush.task.task06.task0612;

/* 
Калькулятор
Сделать класс Calculator, у которого будут 5 статических методов:
int plus(int a, int b) – возвращает сумму чисел a и b
int minus(int a, int b) – возвращает разницу чисел a и b
int multiply(int a, int b) – возвращает результат умножения числа a на число b
double division(int a, int b) – возвращает результат деления числа a на число b
double percent(int a, int b) – возвращает процент b из числа a


Требования:
1. Метод plus должен возвращать сумму чисел a и b.
2. Метод minus должен возвращать разницу чисел a и b.
3. Метод multiply должен возвращать результат умножения числа a на число b.
4. Метод division должен возвращать результат деления числа a на число b.
5. Метод percent должен возвращать процент b из числа a.
*/

public class Calculator {
    public static int plus(int a, int b) {
        //напишите тут ваш код
        return a+b;
    }

    public static int minus(int a, int b) {
        //напишите тут ваш код
        return a-b;
    }

    public static int multiply(int a, int b) {
        //напишите тут ваш код
        return a*b;
    }

    public static double division(int a, int b) {
        //напишите тут ваш код
        return (double)a/b;
    }

    public static double percent(int a, int b) {
        //напишите тут ваш код
        return ((double)b/100)*a;
    }

    public static void main(String[] args) {

    }
}

package com.javarush.task.task06.task0613;

/* 
Кот и статика
В классе Cat создай статическую переменную public int catCount.
Создай конструктор [public Cat()]. Пусть при каждом создании кота 
(нового объекта Cat) статическая переменная catCount увеличивается на 1. 
Создать 10 объектов Cat и вывести значение переменной catCount на экран.


Требования:
1. В классе Cat создай статическую переменную public int catCount.
2. В классе Cat создай создай конструктор public Cat().
3. Конструктор должен увеличивать значение статической переменной catCount на 1.
4. В методе main создай 10 котов.
5. В методе main, после создания котов, выведи значение переменной catCount.
*/

public class Solution {
    public static void main(String[] args) {
        //создай 10 котов
        for(int i=0; i<10; i++){
            Cat newCat = new Cat();
        }
        //выведи значение переменной catCount
        System.out.println(Cat.catCount);
    }

    public static class Cat {
        //создай статическую переменную catCount
        public static int catCount = 0;
        
        //создай конструктор
        public Cat() {
            Cat.catCount++;
        }
    }
}

package com.javarush.task.task06.task0614;

import java.util.ArrayList;

/* 
Статические коты
1. В классе Cat добавь public статическую переменную cats (ArrayList<Cat>).
2. Пусть при каждом создании кота (нового объекта Cat) в переменную cats добавляется этот новый кот. Создать 10 объектов Cat.
3. Метод printCats должен выводить всех котов на экран. Нужно использовать переменную cats.


Требования:
1. Добавь в класс Cat public статическую переменную cats (ArrayList).
2. Переменная cats должна быть проинициализирована.
3. Метод main должен создавать 10 объектов Cat.
4. Метод main должен добавить всех созданных котов в переменную cats.
5. Метод printCats должен выводить всех котов из переменной cats на экран.
Каждого с новой строки.
*/

public class Cat {
    public static ArrayList cats= new ArrayList<>();
 
    public Cat() {
    }
 
    public static void main(String[] args) {
        for (int i = 0; i < 10; i++)
        {
            Cat cat = new Cat();
            cats.add(cat);
        }
        printCats();
    }
 
    public static void printCats() {
        for (int i = 0; i < cats.size(); i++)
        {
            System.out.println(cats.get(i));
        }
    }
}

package com.javarush.task.task06.task0615;

/* 
Феншуй и статики
Переставь один модификатор static, чтобы пример скомпилировался.


Требования:
1. Метод main менять нельзя.
2. Добавь модификатор static в нужное место.
3. Убери лишний модификатор static.
4. В программе должно быть только 2 модификатора static.
*/

public class Solution {

    public static int A = 5;
    public int B = 2;
    public int C = A * B;

    public static void main(String[] args) {
        A = 15;
    }
}

package com.javarush.task.task06.task0616;

/* 
Минимальное число статиков
Расставьте минимальное количество static-ов, чтобы код начал работать, 
и программа успешно завершилась.


Требования:
1. Реализацию методов менять нельзя.
2. Добавь модификаторы static в нужные места.
3. В программе должно быть только 4 модификатора static.
4. Программа должна выводить текст на экран.
*/

public class Solution {
    public static int step;

    public static void main(String[] args) {
        method1();
    }

    public static void method1() {
        method2();
    }

    public static void method2() {
        new Solution().method3();
    }

    public void method3() {
        method4();
    }

    public void method4() {
        step++;
        for (StackTraceElement element : Thread.currentThread().getStackTrace())
            System.out.println(element);
        if (step > 1)
            return;
        main(null);
    }
}

Может кому-то комментарии помогут разобраться со статическими методами :)
/*  Минимальное число статиков */
public class Solution {
    public static int step; // используется в method4 без this, значит переменная static
    public static void main(String[] args) {  method1();  }
      // тут все понятно, static метод main вызывает method1,
      // т.к. статические методы могут вызывать только другие статические методы,
      // значит method1 тоже static
    public static void method1() { method2();  }
      // тут аналогично main, static метод method1 вызывает метод method2,
      // который тоже должен быть static
    public static void method2() {  new Solution().method3();  }
      // тут чуть посложнее, из static метода вызывается method3 путем создания экземпляра 
      // класса Solution, значит method3 не static
    public void method3() { method4();  }
      // тут аналогично main и method1, только нестатический метод вызывает нестатический
}	  
	  
	
package com.javarush.task.task06.task0617;

/* 
Блокнот для новых идей
1. В классе Solution создать public статический класс Idea
2. В классе Idea создать метод public String getDescription(), 
который будет возвращать любой непустой текст
3. В классе Solution создай статический метод 
public void printIdea(Idea idea), 
который будет выводить на экран описание идеи — это то, 
что возвращает метод getDescription


Требования:
1. В классе Solution создать public статический класс Idea.
2. В классе Idea создать метод public String getDescription().
3. Метод getDescription должен возвращать любой непустой текст.
4. В классе Solution создайте статический метод public void printIdea(Idea idea).
5. Метод printIdea должен выводить описание идеи на экран.
*/

public class Solution {
    public static void main(String[] args) {
        printIdea(new Idea());
    }

    //напишите тут ваш код
    public static class Idea{
        public String getDescription(){
            return "New Idea!";
        }
    }
    
    public static void printIdea(Idea idea){
        System.out.println(idea.getDescription());
    }
}

/*
KissMyShinyMetalAss
Создай класс с именем KissMyShinyMetalAss. Создай объект этого класса, выведи его на экран.


Требования:
1. Создай класс KissMyShinyMetalAss внутри класса Solution.
2. Класс KissMyShinyMetalAss должен быть статическим.
3. Класс KissMyShinyMetalAss должен быть public.
4. В методе main создай объект класса KissMyShinyMetalAss.
5. В методе main выведи объект класса KissMyShinyMetalAss на экран.
*/

public class Solution {
    public static class KissMyShinyMetalAss {

    }

    public static void main(String[] args) {
        //напишите тут ваш код
        KissMyShinyMetalAss kissMyShinyMetalAss = new KissMyShinyMetalAss();
        System.out.println(kissMyShinyMetalAss);
    }
}

package com.javarush.task.task06.task0619;

/* 
Три статические переменных name
Добавь 3 public статических переменных: String Solution.name, String Cat.name, String Dog.name.


Требования:
1. В класс Solution добавь public статическую переменную name типа String.
2. В класс Cat добавь public статическую переменную name типа String.
3. В класс Dog добавь public статическую переменную name типа String.
4. В каждом классе должна быть своя переменная name.
*/

public class Solution {
    public static String name;
    
    public static class Cat {
          public static String name;
    }

    public static class Dog {
          public static String name;
    }

    public static void main(String[] args) {

    }
}

Код	Описание
String[] list = new String[5];
Создание массива на 5 элементов типа «строка»
System.out.println(list[0]);
System.out.println(list[1]);
System.out.println(list[2]);
System.out.println(list[3]);
System.out.println(list[4]);
На экран будет выведено пять значений “null”.
Чтобы получить значение, которое хранится в определенной ячейке массива, используйте квадратные скобки и номер ячейки

int listCount = list.length;
listCount получит значение 5 – количество ячеек в массиве list.
list.length хранит длину(количество ячеек) массива.
list[1] = "Mama";
String s = list[1];
При присваивании объектов ячейкам массива нужно указывать индекс/номер ячейки в квадратных скобках.
for (int i = 0; i < list.length; i++)
{
     System.out.println(list[i]);
}
Вывод все

Код	Описание
String[] list = null;
Переменная-массив list, ее значение – null. Она может хранить только контейнер для элементов. Контейнер надо создавать отдельно.
String[] list = new String[5];
Мы создаем контейнер на 5 элементов и кладем ссылку на него в переменную list. Этот контейнер содержит 5 квартир (ячеек) с номерами 0, 1, 2, 3, 4.
String[] list = new String[1];
Мы создаем контейнер на 1 элемент и кладем ссылку на него в переменную list. Чтобы занести что-то в этот контейнер надо написать list[0] = “Yo!”;
String[] list = new String[0];
Мы создаем контейнер на 0 элементов и кладем ссылку на него в переменную list. Ничего в этот контейнер занести нельзя!

Код	Описание
String s;
String[] list;
s равно null
list равно null
list = new String[10];
int n = list.length;
Переменная list хранит ссылку на объект — массив строк из 10 элементов.
n равно 10
list = new String[0];
Теперь list содержит массив из 0 элементов. Массив есть, но хранить элементы он не может.
list = null;
System.out.println(list[1]);
Будет сгенерировано исключение (ошибка программы) – программа аварийно завершится. list содержит пустую ссылку – null
list = new String[10];
System.out.println(list[11]);
Будет сгенерировано исключение (ошибка программы) – выход за границы массива.
Если list содержит 10 элементов/ячеек, то их разрешённые индексы: 0,1,2,3,4,5,6,7,8,9 – всего 10 штук.


Пример 1.
Заполнение массива из 10 чисел, числами от 1 до 10:
public class MainClass
{
    public static void main(String[] args) 
    {
        int[] numbers = new int[10];

        for (int i = 0; i < numbers.length; i++)
        {
           numbers[i] = i + 1;
        }
    }
}
Заполнение массива из 10 чисел, числами от 10 до 1:
public class MainClass
{
    public static void main(String[] args) 
    {
        int[] numbers = new int[10];

        for (int i = 0; i < numbers.length; i++)
        {
           numbers[i] = 10 - i;
        }
    }
}
Заполнение массива из 10 чисел, числами от 0 до 9:
public class MainClass
{
    public static void main(String[] args) 
    {
        int[] numbers = new int[10];

        for (int i = 0; i < numbers.length; i++)
        {
           numbers[i] = i;
        }
    }
}
Заполнение массива из 10 чисел, числами от 9 до 0:
public class MainClass
{
    public static void main(String[] args) 
    {
        int[] numbers = new int[10];

        for (int i = 0; i < numbers.length; i++)
        {
           numbers[i] = 9 - i;
        }
    }
}
Пример 2.
Ввод 10 строк с клавиатуры
public class MainClass
{
  public static void main(String[] args) throws IOException
  {
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    String[] list = new String[10];

    for (int i = 0; i < list.length; i++)
    {
      list[i] = reader.readLine();
     }
  }
}
Ввод 10 чисел с клавиатуры
public class MainClass
{
  public static void main(String[] args) throws IOException
  {
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    int[] list = new int[10];

    for (int i = 0; i < list.length; i++)
    {
      String s = reader.readLine();
      list[i] = Integer.parseInt(s);
    }
  }
}
Пример 3.
Вывод массива на экран
public class MainClass
{
    public static void main(String[] args) throws IOException
    {
        int[] list = new int[10];

        //заполнение массива
        for (int i = 0; i < list.length; i++) 
           list[i] = i;

        //вывод на экран
        for (int i = 0; i < list.length; i++)  
          System.out.println(list[i]);
    }
}
Пример 4.
Быстрая (статическая) инициализация. Сумма элементов массива:
public class MainClass
{
    public static void main(String[] args) throws IOException
    {
        //это статическая инициализация
        int[] list = {5, 6, 7, 8, 1, 2, 5, -7, -9, 2, 0};        
        
        //подсчёт суммы элементов
        int sum = 0;
        for (int i = 0; i < list.length; i++) 
           sum += list[i];

        System.out.println("Sum is " + sum);
    }
}
Пример 5.
Поиск минимального элемента в массиве:
public class MainClass
{
    public static void main(String[] args) throws IOException
    {
        int[] list = {5, 6, 7, 8, 1, 2, 5, -7, -9, 2, 0};

        int min = list[0];
        for (int i = 1; i < list.length; i++)
        {
             if (list[i] < min) 
                  min = list[i];
        }

       System.out.println ("Min is " + min);
    }
}

А можно же минимальный элемент найти проще. 
int[] list = {5, 6, 7, 8, 1, 2, 5, -7, -9, 2, 0};
Arrays.sort(list); //сортировка элементов в массиве по возрастанию.
System.out.println ("Min is " + list[0]);

package com.javarush.task.task07.task0701;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/* 
Массивный максимум
1. В методе initializeArray():
1.1. Создайте массив на 20 чисел
1.2. Считайте с консоли 20 чисел и заполните ими массив
2. Метод max(int[] array) должен находить максимальное число из элементов массива


Требования:
1. Метод initializeArray должен создавать массив из 20 целых чисел.
2. Метод initializeArray должен считать 20 чисел и вернуть их в виде массива.
3. Метод max должен возвращать максимальный элемент из переданного массива.
4. Метод main изменять нельзя.
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        int[] array = initializeArray();
        int max = max(array);
        System.out.println(max);
    }

    public static int[] initializeArray() throws IOException {
        // создай и заполни массив
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int[] array = new int[20];

        for (int i = 0; i < array.length; i++) {
            String s = reader.readLine();
            array[i] = Integer.parseInt(s);
        }
        
        return array;
    }

    public static int max(int[] array) {
        // найди максимальное значение
        int max = array[0];
        for (int i = 1; i < array.length; i++) {
             if (array[i] > max) 
                  max = array[i];
        }
        
        return max;
    }
        
}

package com.javarush.task.task07.task0702;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/* 
Массив из строчек в обратном порядке
1. Создать массив на 10 строк.
2. Ввести с клавиатуры 8 строк и сохранить их в массив.
3. Вывести содержимое всего массива (10 элементов) на экран в обратном порядке. 
Каждый элемент — с новой строки.


Требования:
1. Программа должна создавать массив на 10 строк.
2. Программа должна считывать строки для массива с клавиатуры.
3. Программа должна выводить 10 строк, каждую с новой строки.
4. Все строки массива (10 элементов) должны быть выведены в обратном порядке.
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String[] list = new String[10];

        for (int i = 0; i < 8; i++){
            list[i] = reader.readLine();
        }
        
        for (int i = 0; i < 10; i++){
            System.out.println(list[9-i]);
        }
    }
}

package com.javarush.task.task07.task0703;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/* 
Общение одиноких массивов
1. Создать массив на 10 строк.
2. Создать массив на 10 чисел.
3. Ввести с клавиатуры 10 строк, заполнить ими массив строк.
4. В каждую ячейку массива чисел записать длину строки из массива строк, 
индекс/номер ячейки которой совпадает с текущим индексом из массива чисел.

Вывести содержимое массива чисел на экран, каждое значение выводить с новой строки.


Требования:
1. Программа должна создавать массив на 10 строк.
2. Программа должна создавать массив на 10 целых чисел.
3. Программа должна считывать строки для массива с клавиатуры.
4. Программа должна в массив чисел записать длины строк из массива строк, 
а затем их вывести на экран.
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        String [] str = new String [10];
        int [] num = new int [10];
        
        for (int i = 0; i < 10; i++){
            str[i] = new BufferedReader(new InputStreamReader(System.in)).readLine();
            num[i] = str[i].length();
            System.out.println(num[i]);
        }
    }
}


package com.javarush.task.task07.task0704;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/* 
Переверни массив
1. Создать массив на 10 чисел.
2. Ввести с клавиатуры 10 чисел и записать их в массив.
3. Расположить элементы массива в обратном порядке.
4. Вывести результат на экран, каждое значение выводить с новой строки.


Требования:
1. Программа должна создавать массив на 10 целых чисел.
2. Программа должна считывать числа для массива с клавиатуры.
3. Программа должна выводить 10 строчек, каждую с новой строки.
4. Массив должен быть выведен на экран в обратном порядке.
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int[] num = new int [10];
        for(int i=0; i<10; i++){
            String s = reader.readLine();
            num[9-i] = Integer.parseInt(s);
        }
        for(int i=0; i<10; i++){
            System.out.println(num[i]);
        }
    }
}

package com.javarush.task.task07.task0705;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/* 
Один большой массив и два маленьких
1. Создать массив на 20 чисел.
2. Ввести в него значения с клавиатуры.
3. Создать два массива на 10 чисел каждый.
4. Скопировать большой массив в два маленьких: 
половину чисел в первый маленький, вторую половину во второй маленький.
5. Вывести второй маленький массив на экран, каждое значение выводить с новой строки.


Требования:
1. Программа должна создавать большой массив на 20 целых чисел.
2. Программа должна считывать числа для большого массива с клавиатуры.
3. Программа должна создавать два маленьких массив на 10 чисел каждый.
4. Программа должна скопировать одну половину большого массива в первый маленький, 
а второю - во второй и вывести его на экран.
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int[] num20 = new int [20];
        for(int i=0; i<20; i++){
            String s = reader.readLine();
            num20[i] = Integer.parseInt(s);
        }
        
        int[] num1 = new int [10];
        int[] num2 = new int [10];
        
        for(int i=0; i<10; i++){
            num1[i] = num20 [i];
        }
        for(int i=0; i<10; i++){
            num2[i] = num20 [i+10];
            System.out.println(num2[i]);
        }
    }
}


package com.javarush.task.task07.task0706;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/* 
Улицы и дома
1. Создать массив на 15 целых чисел.
2. Ввести в него значения с клавиатуры.
3. Пускай индекс элемента массива является номером дома, 
а значение — число жителей, проживающих в доме.
Дома с нечетными номерами расположены на одной стороне улицы, 
с четными — на другой. Выяснить, на какой стороне улицы проживает больше жителей.
4. Вывеси на экран сообщение: «В домах с нечетными номерами проживает больше жителей.» 
или «В домах с четными номерами проживает больше жителей.»

Примечание:
дом с порядковым номером 0 считать четным.


Требования:
1. Программа должна создавать массив на 15 целых чисел.
2. Программа должна считывать числа для массива с клавиатуры.
3. Программа должна вывести сообщение 
"В домах с нечетными номерами проживает больше жителей.", 
если сумма нечетных элементов массива больше суммы четных.
4. Программа должна вывести сообщение 
"В домах с четными номерами проживает больше жителей.", 
если сумма четных элементов массива больше суммы нечетных.
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int[] num = new int [15];
        int even = 0;   // четные
        int odd = 0;    // не четные
        
        for(int i=0; i<15; i++){
            String s = reader.readLine();
            num[i] = Integer.parseInt(s);
            
            if(i == 0 || (i % 2 == 0)) 
                even += num[i];
            else
                odd += num[i];
        }
        
        if(even<odd)
            System.out.println("В домах с нечетными номерами проживает больше жителей.");
        else
            System.out.println("В домах с четными номерами проживает больше жителей.");
    }
}



Array															ArrayList
Создание контейнера элементов
String[] list = new String[10];         						ArrayList list = new ArrayList();

Получение количества элементов
int n = list.length;                    						int n = list.size();

Взятие элемента из массива/коллекции 
String s = list[3];                     						String s = list.get(3);

Запись элемента в массив
list[3] = s;                            						list.set(3, s);


Array															ArrayList
Добавление элемента в конец массива
Невозможно выполнить данное действие	 						list.add(s);

Вставка элемента в середину массива			
Невозможно выполнить данное действие							list.add(15, s);

Вставка элемента в начало массива
Невозможно выполнить данное действие							list.add(0, s);

Удаление элемента из массива
Можно стереть элемент с помощью list[3] = null. Но тогда останется «дыра» в массиве.		list.remove(3);



Используем Array												Используем ArrayList
public static void main(String[] args)							public static void main(String[] args)
{																{
Reader r = new InputStreamReader(System.in);					Reader r = new InputStreamReader(System.in);
BufferedReader reader = new BufferedReader(r);					BufferedReader reader = new BufferedReader(r);

//ввод строк с клавиатуры										//ввод строк с клавиатуры
String[] list = new String[10];									ArrayList list = new ArrayList();
for (int i = 0; i < list.length; i++)							for (int i = 0; i < 10; i++)
{																{
  String s = reader.readLine();									  String s = reader.readLine();
  list[i] = s;													  list.add(s);
}																}

//вывод содержимого массива на экран							//вывод содержимого коллекции на экран 
for (int i = 0; i < list.length; i++)							for (int i = 0; i < list.size(); i++)
{																{
  int j = list.length - i - 1;									  int j = list.size() - i - 1;
  System.out.println( list[j] );								  System.out.println( list.get(j) );
}																}
}																}


package com.javarush.task.task07.task0707;

import java.util.ArrayList;

/* 
Что за список такой?
1. Создай список строк.
2. Добавь в него 5 различных строк.
3. Выведи его размер на экран.
4. Используя цикл выведи его содержимое на экран, каждое значение с новой строки.


Требования:
1. Программа не должна ничего считывать с клавиатуры.
2. Объяви переменную типа ArrayList (список строк) и сразу проинициализируй ee.
3. Программа должна добавить 5 любых строк в список. Строки придумай сам.
4. Программа должна вывести размер списка на экран.
5. Программа должна вывести содержимое списка на экран, каждое значение с новой строки.
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        /*ArrayList list = new ArrayList();
        
        for (int i = 0; i < 5; i++){
        String s = "string" + i;
        list.add(s);					
        }
        System.out.println( list.size() );
        
        for (int i = 0; i < list.size(); i++){
            int j = list.size() - i - 1;
            System.out.println( list.get(j) );
        }
        */
        ArrayList<String> list = new ArrayList<>();
        list.add("aaa");
        list.add("bbb");
        list.add("ccc");
        list.add("ddd");
        list.add("eee");

        int n = list.size();
        System.out.println(n);
        for (int i = 0; i < n; i++)
        {
            System.out.println( list.get(i) );
        }
    }
}

package com.javarush.task.task07.task0708;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

/* 
Самая длинная строка
1. Создай список строк.
2. Считай с клавиатуры 5 строк и добавь в список.
3. Используя цикл, найди самую длинную строку в списке.
4. Выведи найденную строку на экран.
5. Если таких строк несколько, выведи каждую с новой строки.


Требования:
1. Объяви переменную типа ArrayList (список строк) и сразу проинициализируй ee.
2. Программа должна считывать 5 строк с клавиатуры и записывать их в список.
3. Программа должна выводить самую длинную строку на экран.
4. Если есть несколько строк с длиной равной максимальной, 
то нужно вывести каждую из них с новой строки.
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        
        ArrayList<String> list = new ArrayList<>();
        
        for (int i = 0; i < 5; i++)
            list.add(reader.readLine());

        int max = list.get(0).length();
        for (int i = 0; i < list.size(); i++)
            if (list.get(i).length() > max)
                max = list.get(i).length();

        for (int i = 0; i < 5; i++)
            if (max == list.get(i).length()) 
                System.out.println(list.get(i));
    }
}

package com.javarush.task.task07.task0709;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

/* 
Выражаемся покороче
1. Создай список строк.
2. Считай с клавиатуры 5 строк и добавь в список.
3. Используя цикл, найди самую короткую строку в списке.
4. Выведи найденную строку на экран.
5. Если таких строк несколько, выведи каждую с новой строки.


Требования:
1. Объяви переменную типа список строк и сразу проинициализируй ee.
2. Программа должна считывать 5 строк с клавиатуры и записывать их в список.
3. Программа должна выводить самую короткую строку на экран.
4. Если есть несколько строк с длиной равной минимальной, 
то нужно вывести каждую из них с новой строки.

*/

public class Solution {
	public static void main(String[] args) throws Exception {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        ArrayList<String> list = new ArrayList<>();
        
        for (int i = 0; i < 5; i++)
            list.add(reader.readLine());

        int min = list.get(0).length();
        for (int i = 0; i < list.size(); i++)
            if (list.get(i).length() < min)
                min = list.get(i).length();

        for (int i = 0; i < 5; i++)
            if (min == list.get(i).length()) 
                System.out.println(list.get(i));
	}
}


package com.javarush.task.task07.task0710;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

/* 
В начало списка
1. Создай список строк в методе main.
2. Добавь в него 10 строчек с клавиатуры, но только добавлять не в конец списка, а в начало.
3. Используя цикл выведи содержимое на экран, каждое значение с новой строки.


Требования:
1. Объяви переменную типа список строк и сразу проинициализируй ee.
2. Программа должна считывать 10 строк с клавиатуры и добавлять их в список.
3. Программа должна добавлять строки в начало списка.
4. Программа должна выводить список на экран, каждое значение с новой строки.
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        ArrayList<String> list = new ArrayList<>();
        
        for (int i = 0; i < 10; i++) 
			list.add(0, reader.readLine());
		
		for (int i = 0; i < 10; i++) 
			System.out.println(list.get(i));
    }
}

package com.javarush.task.task07.task0711;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

/* 
Удалить и вставить
1. Создай список строк.
2. Добавь в него 5 строк с клавиатуры.
3. Удали последнюю строку и вставь её в начало. Повторить 13 раз.
4. Используя цикл выведи содержимое на экран, каждое значение с новой строки.


Требования:
1. Объяви переменную типа список строк и сразу проинициализируй ee.
2. Программа должна считывать 5 строк с клавиатуры и добавлять их в список.
3. Удали последнюю строку и вставь её в начало. Повторить 13 раз.
4. Программа должна выводить список на экран, каждое значение с новой строки.
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        ArrayList<String> list = new ArrayList<>();
        
        for (int i = 0; i < 5; i++)
			list.add(i, reader.readLine());
		
		for (int i = 0; i < 13; i++) {
		    //String a = list.remove(4);
		    //list.add(0, a);
		    list.add(0, list.remove(4));
		}
		
		for (int i = 0; i < 5; i++) 
			System.out.println(list.get(i));
    }
}

package com.javarush.task.task07.task0712;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/* 
Самые-самые
1. Создай список строк.
2. Добавь в него 10 строчек с клавиатуры.
3. Узнай, какая строка в списке встретится раньше: самая короткая или самая длинная.
Если таких строк несколько, то должны быть учтены самые первые из них.
4. Выведи на экран строку из п.3.


Требования:
1. Объяви переменную типа список строк и сразу проинициализируй ee.
2. Программа должна считывать 10 строк с клавиатуры и добавлять их в список.
3. Программа должна выводить на экран самую короткую строку, если она была раньше самой длинной.
4. Программа должна выводить на экран самую длинную строку, если она была раньше самой короткой.
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        ArrayList<String> list = new ArrayList<>();
        
        for (int i = 0; i < 10; i++)
			list.add(i, reader.readLine());
		
		int min = list.get(0).length();
		int max = list.get(0).length();
		int countMin = 0;
		int countMax = 0;
        for (int i = 0; i < list.size(); i++)
            if (list.get(i).length() < min){
                min = list.get(i).length();
                countMin = i;
            } else if(list.get(i).length() > max){
                max = list.get(i).length();
                countMax = i;
            }

        for (int i = 0; i < 10; i++){
            if(countMin < countMax){
                    if (min == list.get(i).length()) {
                        System.out.println(list.get(i));
                        break;
                    }
            }else{
                if (max == list.get(i).length()) {
                        System.out.println(list.get(i));
                        break;
                }
            }
        }
    }
}


ArrayList<String> list = new ArrayList<String>();
Мы создали переменную list типа ArrayList.
Занесли в нее объект типа ArrayList.
В таком списке можно хранить только переменные типа String.

ArrayList list = new ArrayList();
Мы создали переменную list типа ArrayList.
Занесли в нее объект типа ArrayList.В таком листе можно хранить переменные любого типа.

ArrayList<Integer> list = new ArrayList<Integer>();
Мы создали переменную list типа ArrayList.
Занесли в нее объект типа ArrayList.
В таком листе можно хранить только переменные типа Integer и int.


Примитивный тип	Класс		Список
int				Integer		ArrayList<Integer>
double			Double		ArrayList<Double>
boolean			Boolean		ArrayList<Boolean>
char			Character	ArrayList<Character>
byte			Byte		ArrayList<Byte>

 Примитивные типы и их классы-аналоги (классы-обёртки) можно спокойно присваивать друг другу:

Примеры
int a = 5;
Integer b = a;
int c = b;

Character c = 'c'; //литерал 'c' имеет тип char
char d = c;

Byte b = (byte) 77; //литерал 77 имеет тип int

Boolean isOk = true; //литерал true имеет тип boolean

Double d = 1.0d; //литерал 1.0d имеет тип double


 Пример 1:
Ввод списка целых чисел с клавиатуры
public static void main(String[] args) throws IOException
{
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in) );
    ArrayList list = new ArrayList();

    while (true)
    {
        String s = reader.readLine();
        if (s.isEmpty()) break;
        list.add(Integer.parseInt(s));
    }
}
	
Пример 2:
То же, чётные числа добавляются в конец списка, нечётные – в начало.
public static void main(String[] args) throws IOException
{
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    ArrayList list = new ArrayList();

    while (true)
    {
        String s = reader.readLine();
        if (s.isEmpty()) break;

        int x = Integer.parseInt(s);
        if (x % 2 == 0)  //проверяем, что остаток от деления на два равен нулю
            list.add(x);          //добавление в конец
        else
            list.add(0, x);      //вставка в начало      
    }
}

— Пример 3:
Удаление всех чисел больше 5:
public static void main(String[] args) throws IOException
{
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in) );
    ArrayList list = new ArrayList();

    list.add(1);
    list.add(7);
    list.add(11);
    list.add(3);
    list.add(15);

    for (int i = 0; i < list.size(); ) //убрали увеличение i внутрь цикла 
    { 
        if (list.get(i) > 5)
            list.remove(i);  //не увеличиваем i, если удалили текущий  элемент  
        else
            i++;
    }
}

 Пример 4:
Разделение массива на два – чётных и нечётных чисел
public static void main(String[] args) throws IOException
{
    //статическая инициализация массива
    int[] data = {1, 5, 6, 11, 3, 15, 7, 8};  

    //создание списка, где все элементы должны быть типа Integer 
    ArrayList list = new ArrayList();   

    //заполнение списка из массива
    for (int i = 0; i < data.length; i++) list.add(data[i]);  

    ArrayList even = new ArrayList();  //чётные
    ArrayList odd = new ArrayList();    //нечётные

    for (int i = 0; i < list.size(); i++)
    {
        Integer x = list.get(i);
        if (x % 2 == 0)    //если x - чётное
            even.add(x);   // добавляем x в коллекцию четных чисел  
        else
            odd.add(x);    // добавляем x в коллекцию нечетных чисел
    }
}	  
	  
Пример 5:

Слияние списков.
public static void main(String[] args) throws IOException
{
    ArrayList list1 = new ArrayList();   //создание списка  
    Collections.addAll(list1, 1, 5, 6, 11, 3, 15, 7, 8);   //заполнение списка

    ArrayList list2 = new ArrayList();
    Collections.addAll(list2, 1, 8, 6, 21, 53, 5, 67, 18);

    ArrayList result = new ArrayList();

    result.addAll(list1);   //добавление всех значений из одного списка в другой
    result.addAll(list2);

    for (Integer x : result)   //быстрый for по всем элементам, только для коллекций
    {
        System.out.println(x);
    }
}	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  